import React, { useRef, useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonDatetime,
  IonGrid,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";
import { useHistory } from "react-router";
import LocalizedStrings from "react-localization";

const RegistrationStep2: React.FC = (props) => {
  let history: any = useHistory();
  const [FirstNameError, setFirstNameError] = useState<string>("");
  const [LastNameError, setLastNameError] = useState<string>("");
  const [DobError, setDobError] = useState<string>("");

  console.log(
    "[registration slide 2]: printing details from step 1",
    history.location.state?.qatarIdInputRef
  );

  let firstNameInputRef = useRef<HTMLIonInputElement>(null);
  let lastNameInputRef = useRef<HTMLIonInputElement>(null);

  const getUserData = () => {
    console.log("[Registration step 2: inside getUserDate funtion]");

    return {
      firstNameInputRef: firstNameInputRef.current!.value!,
      lastNameInputRef: lastNameInputRef.current!.value!,
      dobRef: dobRef.current!.value!,
      qatarIdInputRef: history.location.state.qatarIdInputRef,
      phoneNumberInputRef: history.location.state.phoneNumberInputRef,
    };
  };

  const dobRef = useRef<HTMLIonDatetimeElement>(null);
  const [age, ageChange] = useState<number>(0);

  const calculateAge = () => {
    var calAge: number = 0;
    var calMonth: number = 0;

    const dobString: string = dobRef.current!.value!;
    const dob: Date = new Date(dobString);
    const yearDob: number = dob.getFullYear();
    const monthDob: number = dob.getMonth();

    const today: Date = new Date();
    const yearNow: number = today.getFullYear();
    const monthNow: number = today.getMonth();

    calAge = yearNow - yearDob;
    calMonth = monthNow - monthDob;

    if (calMonth < 0 || (calMonth === 0 && today.getDate() < dob.getDate())) {
      calAge--;
    }

    if (calAge < 0) calAge = 0;

    ageChange(calAge);
    console.log(age);
  };

  let strings = new LocalizedStrings({
    en: {
      registration: "Registration",
      step2: "Step 2",
      firstName: "First Name",
      lastName: "Last Name",
      dateOfBirth: "Date Of Birth",
      selectDate: "Select Date",
      age: "Age",
      next: "Next",
    },
    ar: {
      registration: "مستشفى",
      step2: "مستشفى",
      firstName: "مستشفى",
      lastName: "مستشفى",
      dateOfBirth: "مستشفى",
      selectDate: "مستشفى",
      age: "مستشفى",
      next: "مستشفى",
    },
  });
  const FirstNameOnFocus = (event: any) => {
    setFirstNameError(" ");
  };
  const LastNameOnFocus = (event: any) => {
    setLastNameError(" ");
  };
  const DobOnFocus = (event: any) => {
    setDobError(" ");
  };

  const ValidateFields = () => {
    if (
      firstNameInputRef.current!.value != "" &&
      lastNameInputRef.current!.value != "" &&
      (dobRef.current!.value != "" && dobRef.current!.value != undefined )
    ) {
      history.push({ pathname: "/RegistrationStep3", state: getUserData() });
    } else {
      if (firstNameInputRef.current!.value != "" && firstNameInputRef.current!.value != undefined) {
        setFirstNameError("");
      } else {
        setFirstNameError("This field is Required");
        return false;
      }

      if (lastNameInputRef.current!.value != "") {
        setLastNameError("");
      } else {
        setLastNameError("This field is Required");
        return false;
      }

      if (dobRef.current!.value != null &&  dobRef.current!.value != undefined) {
        setDobError("");
      } else {
        setDobError("This field is Required");
        return false;
      }
    }
  };

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="headergrid">
        <br/>

          <IonRow>
            <IonBackButton
              defaultHref="RegistrationStep1"
              color="light"
              class="backbutton"
            />
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>{strings.registration}</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card" style={{height:"400px"}}>
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>{strings.step2}</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        <IonLabel class="inputlabel">
                          {strings.firstName}
                        </IonLabel>
                        <IonInput
                          placeholder={strings.firstName}
                          clear-input={true}
                          autofocus={true}
                          type="text"
                          ref={firstNameInputRef}
                          onFocus={FirstNameOnFocus}
                        ></IonInput>
                        {FirstNameError && (
                          <IonText color="danger">
                            <p>{FirstNameError}</p>
                          </IonText>
                        )}
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <IonLabel class="inputlabel">
                          {strings.lastName}
                        </IonLabel>
                        <IonInput
                          placeholder={strings.lastName}
                          clear-input={true}
                          autofocus={true}
                          type="text"
                          ref={lastNameInputRef}
                          onFocus={LastNameOnFocus}
                        ></IonInput>
                        {LastNameError && (
                          <IonText color="danger">
                            <p>{LastNameError}</p>
                          </IonText>
                        )}
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol size="9">
                        <IonLabel class="inputlabel">
                          {strings.dateOfBirth}
                        </IonLabel>
                        <IonDatetime
                          class="dob"
                          ref={dobRef}
                          placeholder={strings.selectDate}
                          displayFormat="MMM DD, YYYY"
                          onIonChange={calculateAge}
                          onFocus={DobOnFocus}
                        ></IonDatetime>
                        {DobError && (
                          <IonText color="danger">
                            <p>{DobError}</p>
                          </IonText>
                        )}
                      </IonCol>
                      <IonCol size="3">
                        <IonLabel class="inputlabel">{strings.age}</IonLabel>
                        <IonInput class="dob" readonly value={age}></IonInput>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        class="nextBtn"
                        strong={true}
                        onClick={ValidateFields}
                      >
                        {strings.next}
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep2;

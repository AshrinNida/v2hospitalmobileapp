import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonText,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./BookAnAppointmentStep1.css";
import Doctor from "../images/Doctor-01.png";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import LocalizedStrings from "react-localization";
import { useHistory, useParams } from "react-router";
import { presentToastWithOptions } from "../shared/toast/toast";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil"

const options = {
  cssClass: "my-custom-interface",
};

const BookAnAppointmentStep1: React.FC = () => {
  const departmentValue = useParams<{ department: string }>().department;
  var temp_offerings: string[] = [];
  let arrayOfDetails: Idetails[] = [];
  let inital_index: number = 0;
  const [offerings, setOfferings] = useState<string[]>([]);
  const [details, setdetails] = useState<Idetails[]>([]);
  const [value, setValue] = useState<string>();
  let history: any = useHistory();

  interface Idetails {
    name: string;
    department: string;
    cliniccode: number;
    iconURL: string;
  }

  const PopulateDepartment = (data: any) => {
    arrayOfDetails = [];
    temp_offerings = [];
    data.forEach((d: any) => {
      temp_offerings.push(d.name);
      d.doctors.forEach((doc: any) => {
        arrayOfDetails.push({
          name: doc.name,
          department: d.name,
          cliniccode: doc.clinicCode,
          iconURL: doc.iconURL,
        });
      });
    });
    console.log(data)
    console.log(temp_offerings)
    temp_offerings.sort((a, b) => a.localeCompare(b))
    setOfferings(temp_offerings);
    setdetails(arrayOfDetails);
    inital_index =
      departmentValue !== "" && temp_offerings.indexOf(departmentValue) >= 0
        ? temp_offerings.indexOf(departmentValue)
        : 0;
    setValue(temp_offerings[inital_index]);
  };

  const GetDoctorList = () => {
    let headers = getAuthHeader();

    axios
      .get<any>(endpoints.hostName + apiPath.DoctorsListApiPath, { headers }) //"http://34.131.82.122:3001/alemadi/mobile/doctor/list")
      .then(
        (response) => {
          PopulateDepartment(response.data.departments);
        },
        (error) => {
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        }
      );
  };

  useEffect(() => {
    GetDoctorList();
  }, []);

  const updateIndex = (e: any) => {
    setValue(e.detail.value);
  };

  let strings = new LocalizedStrings({
    en: {
      bookAppointment: "Book Appointment ",
    },
    ar: {
      bookAppointment: "مستشفى",
    },
  });

  return (
    <IonPage>
      <Header title={strings.bookAppointment} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="b1-grid">
          <IonRow class="b1-selectRow">
            <IonCol>
              <IonSelect
                interfaceOptions={options}
                class="b1-select"
                value={value}
                onIonChange={updateIndex}
              >
                {offerings.map((offerings, i) => (
                  <IonSelectOption value={offerings} key={i}>
                    {offerings}
                  </IonSelectOption>
                ))}{" "}
              </IonSelect>
            </IonCol>
          </IonRow>
          {details
            .filter((i) => i.department === value)
            .map((item, i) => (
              <IonRow class="b1-row" key={i}>
                <IonCol size="2">
                  <IonImg src={endpoints.hostName + item.iconURL}></IonImg>
                </IonCol>
                <IonCol size="6" class="b1-textCol">
                  <IonText class="b1-text1">
                    <b className="fontHeading">{item.name}</b>
                  </IonText>
                  <IonText class="b1-text2">{item.department}</IonText>
                </IonCol>
                <IonCol size="4">
                  <IonButton
                    size="small"
                    class="b1-btn"
                    routerLink={`/BookAnAppointmentStep2/${item.cliniccode}`}
                  >
                    {strings.bookAppointment}
                  </IonButton>
                </IonCol>
              </IonRow>
            ))}
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep1;

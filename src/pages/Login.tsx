import {
  IonAvatar,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonIcon,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
  useIonRouter,
} from "@ionic/react";
import React, { useRef, useState } from "react";
import "./Login.css";
import logo from "../images/defaultProfileImage.png";
import axios from "axios";
import { checkmarkCircleOutline } from "ionicons/icons";
import { endpoints, apiPath, tokenKey } from "../shared/application_constants";

import LocalizedStrings from "react-localization";
import { presentToastWithOptions } from "../shared/toast/toast";
import { Network } from "@capacitor/network";
import { STATUS_CODES } from "http";


const Login1: React.FC = () => {
  const errorObj = { isError: false, errorMsg: "" };
  const [disableLoginBtn, setdisableLoginBtn] = useState(true);
  const [otpArray, setOtpArray] = useState(["", "", "", ""]);
  const [IsValidQID, setIsValidQID] = useState(true);
  const [patcode, setPatcode] = useState(null);
  const router = useIonRouter();
  const [QidErrorObj, setQidErrorObj] = useState(errorObj);
  const [otpErrorObj, setotpErrorObj] = useState(errorObj);
  const [disableGenerateOtpBtn, setdisableGenerateOtpBtn] = useState(true);
  const [showToast, setShowToast] = useState<boolean>(false);


  // shared
  console.log(
    "[Login page]: printing endpoint",
    endpoints.hostName,
    apiPath.loginApiPath
  );

  const handleChange = (event: any) => {
    const pattern3 = /^[2-3][0-9.,]/;
    userName = userNameInputRef.current!.value!.toLocaleString();
    mobileNumberInputRef.current!.value = "";
    setIsValidQID(true);
    setQidErrorObj(errorObj); //reset QID error object
    // setdisableLoginBtn(true);
    setdisableGenerateOtpBtn(true);
    if (userName.length == 11) {
      if (!pattern3.test(userName)) {
        setIsValidQID(true);
        setQidErrorObj({
          isError: true,
          errorMsg: "Please enter valid Qatar Id",
        });
        return;
      } else {
        axios
          .get(endpoints.hostName + apiPath.validateQIdApiPath + userName)
          .then((response) => {
            if (response && response.data && response.data["isQIDExist"]) {
              setIsValidQID(false);
              setdisableGenerateOtpBtn(false);

              //first 6 digits of Mobile Number hidden
              let a: string = response.data["MOBILE"];
              console.log(a.toString().length);
              let chars = a.toString().split("");

              for (let i = 0; i < 6; i++) {
                chars[i] = "X";
              }
              a = chars.join("");
              console.log(a);
              mobileNumberInputRef.current!.value = a = chars.join("");
              localStorage.setItem(
                "userDetails",
                JSON.stringify(response.data)
              );
            } else {
              setIsValidQID(true);
              setQidErrorObj({
                isError: true,
                errorMsg: "QID does not exist.",
              });
              console.error("[Login page]: QID does not exist");
            }
          })
          .catch((error) => {
            setQidErrorObj({
              isError: true,
              errorMsg: "Please enter valid Qatar Id",
            });
            console.error("[Login page]: verify QID Error ApiFail");
          });
      }
    }
  };

  Network.addListener('networkStatusChange', status => {
    console.log('Network status changed', status);
       if (status.connected){
        setShowToast(true);
        presentToastWithOptions('Network Connected');
      }else{
        setShowToast(false);
        presentToastWithOptions('Network was disconnected','error');
      }
      });

  const userNameInputRef = useRef<HTMLIonInputElement>(null);
  const mobileNumberInputRef = useRef<HTMLIonInputElement>(null);

  let userName: string;

  const getOtp = () => {
    const otpValues = Object.values(otpArray);
    userName = userNameInputRef.current!.value!.toLocaleString();
    const pattern3 = /^[2-3][0-9.,]/;
    if (!userName || userName == "" || !pattern3.test(userName)) {
      console.log("invalid qid");
      return;
    }

    let reqdata = {
      // "qatarId": 28535659269
      qatarId: userName,
    };
    axios.post(endpoints.hostName + apiPath.sendOtpApiPath, reqdata).then(
      (response: any) => {
        if (response && response.data && response.data["PATCODE"]) {
          setdisableLoginBtn(false);
          setIsValidQID(false);
          setPatcode(response.data["PATCODE"]);
        }
        console.log("[Login page]: generate otp successful");
      },
      (error) => {
        setdisableLoginBtn(true);
        console.log("[Login page]: generate otp Error");
      }
    );
  };

  const ValidateQID = (event: any) => {
    axios
      .post("http://34.131.82.122:3001/alemadi/mobile/patient/otp/send", {
        qatarId: 28535659260,
      })
      .then(
        (response) => {
          console.log(response);
          setIsValidQID(false);
        },
        (error) => {
          console.log(error);
          setIsValidQID(true);
        }
      );
    console.log(userName);
    const pattern3 = /^[2-3][0-9.,]/;
    if (!userName || userName == "" || !pattern3.test(userName)) {
      console.log("invalid qid");
      return;
    }
    let reqdata = {
      // "qatarId": 28535659269
      qatarId: userName,
    };
    axios.post(endpoints.hostName + apiPath.sendOtpApiPath, reqdata).then(
      (response: any) => {
        if (response && response.data && response.data["PATCODE"]) {
          setPatcode(response.data["PATCODE"]);
        }
        console.log("[Login page]: generate otp successful");
      },
      (error) => {
        console.log("[Login page]: generate otp Error");
      }
    );
  };

  const verifyOtp = () => {
    setotpErrorObj(errorObj);
    let otpString = otpArray.toString().replaceAll(",", "");
    if (!otpString || otpString == "" || otpString.length !== 4) {
      console.log("otp not entered");
      setotpErrorObj({ isError: true, errorMsg: "Please enter valid Otp." });
      return;
    }
    let AuthToken : string = ' ';
    let data = {
      PATCODE: patcode,
      OTP: otpString,
      token: AuthToken,
    };
    axios.post(endpoints.hostName + apiPath.verifyOtpApiPath, data).then(
      (response) => {
        if (response && response.data) {
         
         AuthToken = response.data.token;
          localStorage.setItem(tokenKey.AUTH_TOKEN_KEY,response.data.token);
          console.log('token :',AuthToken);

          console.log("[Login page]: otp verification Pass");
          presentToastWithOptions('Logged In Successfully.');
          router.push("/main", "none", "replace");
        } else {
          setotpErrorObj({
            isError: true,
            errorMsg: "Please enter valid Otp.",
          });
          console.error("[Login page]: otp verification return false");
          presentToastWithOptions('Login Failed.');

        }
      },
      (error) => {
        setotpErrorObj({ isError: true, errorMsg: "Please enter valid Otp." });
        console.log("[Login page]: otp verification Error api");
      }
    );
  };

  const AuthenticateUser = () => {
    userName = userNameInputRef.current!.value!.toLocaleString();
    const valuesOnly = Object.values(otpArray);
    console.log(valuesOnly.toString().replaceAll(",", "").length);
    console.log(userName);
  };

  const numberOnlyValidation = (event: any) => {
    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  };

  const QIdValidation = (event: any) => {
    const pattern2 = /[0-9.,]/;
    const pattern3 = /^[2-3][0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);
    console.log(pattern3.test(event.target.value));
    if (!event.target.value || event.target.value == "") {
      if (inputChar != "2" && inputChar != "3") {
        event.preventDefault();
      }
    } else if (!pattern2.test(inputChar)) {
      event.preventDefault();
    }
  };

  //change otp
  const onChange = (event: any) => {
    setotpErrorObj(errorObj);
    let index = event.target.id;
    console.log(index);
    if (event.target.value) {
      const otpArrayCopy = otpArray.concat();
      otpArrayCopy[Number(index)] = event.target.value;
      setOtpArray(otpArrayCopy);
      if (index === "0") {
        document.getElementById("1")?.focus();
      } else if (index === "1") {
        document.getElementById("2")?.focus();
      } else if (index === "2") {
        document.getElementById("3")?.focus();
        // do the rest here
      }
    } else {
      const otpArrayCopy = otpArray.concat();
      otpArrayCopy[Number(index)] = ""; // clear the previous box which will be in focus
      setOtpArray(otpArrayCopy);
    }
  };

  let strings = new LocalizedStrings({
    en: {
      hospitalName: "Al Emadi Hospital - Doha, Qatar",
      loginTo: "LOGIN TO",
      boiledEgg: "Boiled egg",
      softBoiledEgg: "Soft-boiled egg",
      choice: "How to choose the egg",
      qidNumber: "QID Number",
      mobileNumber: "Mobile Number",
      otpNumber: "OTP NUmber",
      generateOTP: "Generate OTP",
      rememberMe: "Remember Me",
      login: "Login",
      orRegisterHere: " Or Register Here",
    },
    ar: {
      hospitalName: "مستشفى",
      loginTo: "مستشفى",
      boiledEgg: "مستشفى",
      softBoiledEgg: "مستشفى",
      choice: "مستشفى",
      qidNumber: "مستشفى",
      mobileNumber: "مستشفى",
      otpNumber: "مستشفى",
      generateOTP: "مستشفى",
      rememberMe: "مستشفى",
      login: "مستشفى",
      orRegisterHere: "مستشفى",
    },
  });
  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol><br/>
              <h3 className="loginLabel">
                <IonText> {strings.loginTo}</IonText>
              </h3>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonCard class="card">
                <IonCardHeader>
                  <IonAvatar class="profileImage-center">
                    <img src={logo} alt="logo missing" />
                  </IonAvatar>
                </IonCardHeader>
                <IonCardContent class="cardContent">
                  <IonGrid>
                    <IonRow>
                      <IonCol size="10" offset="1">
                        <IonLabel class="inputlabel">
                          {strings.qidNumber}
                        </IonLabel>
                        <IonInput
                          type="tel"
                          placeholder={strings.qidNumber}
                          clear-input={true}
                          autofocus={true}
                          ref={userNameInputRef}
                          pattern="/[0-9.,]/"
                          maxlength={11}
                          onIonChange={(e) => handleChange(e.detail.value!)}
                          onKeyPress={QIdValidation}
                        >
                          <label hidden={IsValidQID}>
                            <div>
                              <IonIcon
                                icon={checkmarkCircleOutline}
                                slot="end"
                                size="large"
                                className="checkmark"
                              />
                            </div>
                          </label>
                        </IonInput>
                        {QidErrorObj.isError && (
                          <IonText class="error">
                            {QidErrorObj.errorMsg}
                          </IonText>
                        )}
                      </IonCol>
                      <IonCol size="10" offset="1">
                        <IonLabel class="inputlabel">
                          {strings.mobileNumber}
                        </IonLabel>
                        <IonInput
                          readonly
                          placeholder={strings.mobileNumber}
                          clear-input={true}
                          autofocus={true}
                          ref={mobileNumberInputRef}
                          type="tel"
                          onKeyPress={numberOnlyValidation}
                          maxlength={8}
                        ></IonInput>

                        <IonButton
                          size="small"
                          disabled={disableGenerateOtpBtn}
                          class="otpbtn"
                          onClick={getOtp}
                        >
                          {strings.generateOTP}
                        </IonButton>
                      </IonCol>
                      <br />
                      <br />
                      <IonCol size="10" offset="1" class="center">
                        <IonLabel class="inputlabel">
                          {strings.otpNumber}
                        </IonLabel>
                        <input
                          value={otpArray[0]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="0"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[1]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="1"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[2]}
                          className="otpInput"
                          maxLength={1}
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="2"
                          onKeyPress={numberOnlyValidation}
                        ></input>
                        <input
                          value={otpArray[3]}
                          className="otpInput"
                          placeholder="0"
                          type="tel"
                          onChange={onChange}
                          id="3"
                          maxLength={1}
                          onKeyPress={numberOnlyValidation}
                        ></input>
                      </IonCol>
                    </IonRow>
                    <IonRow class="center">
                      {otpErrorObj.isError && (
                        <IonText class="error center">
                          {otpErrorObj.errorMsg}
                        </IonText>
                      )}
                    </IonRow>
                    <br></br>
                    <IonRow>
                      <IonCol class="loginbutton">
                        <IonButton
                          expand="block"
                          onClick={verifyOtp}
                          disabled={disableLoginBtn}
                        >
                          {strings.login}
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <h3 className="h2BottomText">
                <a href="/RegistrationStep1" className="anchorWhite">
                  <IonText> {strings.orRegisterHere}</IonText>
                </a>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login1;

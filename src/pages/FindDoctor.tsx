import {
  IonCol,
  IonContent,
  IonGrid,
  IonLabel,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonSearchbar,
} from "@ionic/react";
import React, { useState, useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import location from "../images/location.png";
import INTERNAL_MEDICINE from "../images/INTERNAL_MEDICINE.png";
import EXTERNAL_REFERRAL_DOCTORS from "../images/EXTERNAL_REFERRAL_DOCTORS.png";
import PHYSIOTHERAPY_MAIN_HOSPITAL from "../images/PHYSIOTHERAPY_MAIN_HOSPITAL.png";
import GENERAL_AND_BARATRIC_SURGERY from "../images/GENERAL_AND_BARATRIC_SURGERY.png";
import OBSTETRICS_AND_GYNECOLOGY from "../images/OBSTETRICS_AND_GYNECOLOGY.png";
import ORTHOPEDIC from "../images/ORTHOPEDIC.png";
import ENT from "../images/ENT.png";
import DERMATOLOGY from "../images/DERMATOLOGY.png";
import CARDIOLOGY from "../images/CARDIOLOGY.png";
import PLASTIC_SURGERY from "../images/PLASTIC_SURGERY.png";
import DENTAL from "../images/DENTAL.png";
import OPHTHALMOLOGY from "../images/OPHTHALMOLOGY.png";
import DIETARY from "../images/DIETARY.png";
import UROLOGY from "../images/UROLOGY.png";
import PHYSIOTHERAPY_CENTER from "../images/PHYSIOTHERAPY_CENTER.png";
import PEDIATRICS from "../images/PEDIATRICS.png";
import "./FindDoctor.css";
import LocalizedStrings from "react-localization";
import axios from "axios";
import { endpoints, apiPath } from "../shared/application_constants";
import { presentToastWithOptions } from "../shared/toast/toast";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import { useHistory } from "react-router";

const FindDoctor: React.FC = () => {
  var temp_offerings: string[] = [];
  let arrayOfServices1: Idetails[] = [];
  const [searchText, setSearchText] = useState("");
  const [arrayOfServices, setarrayOfServices] = useState<Idetails[]>([]);
  const [Hospitallocation, setHospitallocation] = useState<string>("2");
  let history: any = useHistory();


  interface Idetails {
    icon: any;
    url: string;
    department: string;
    location: string;
  }

  let departments = new Map<string, any>([
    ["INTERNAL MEDICINE", INTERNAL_MEDICINE],
    ["EXTERNAL REFERRAL DOCTORS", EXTERNAL_REFERRAL_DOCTORS],
    ["PHYSIOTHERAPY MAIN HOSPITAL", PHYSIOTHERAPY_MAIN_HOSPITAL],
    ["GENERAL AND BARATRIC SURGERY", GENERAL_AND_BARATRIC_SURGERY],
    ["OBSTETRICS AND GYNECOLOGY", OBSTETRICS_AND_GYNECOLOGY],
    ["ORTHOPEDIC", ORTHOPEDIC],
    ["ENT", ENT],
    ["DERMATOLOGY", DERMATOLOGY],
    ["CARDIOLOGY", CARDIOLOGY],
    ["PLASTIC_SURGERY", PLASTIC_SURGERY],
    ["DENTAL", DENTAL],
    ["OPHTHALMOLOGY", OPHTHALMOLOGY],
    ["DIETARY", DIETARY],
    ["UROLOGY", UROLOGY],
    ["PHYSIOTHERAPY CENTER", PHYSIOTHERAPY_CENTER],
    ["PEDIATRICS", PEDIATRICS],
  ]);

  let PopulateServices = (data: any) => {
    arrayOfServices1 = [];
    temp_offerings = [];
    console.log();
    data.forEach((d: any) => {
      if (departments.has(d.name.toString())) {
        temp_offerings.push(d.name);
        arrayOfServices1.push({
          icon: departments.get(d.name.toString()),
          url: "/BookAnAppointmentStep1",
          department: d.name,
          location: d.doctors.some(
            (e: { location: { id: string } }) => e.location.id === "2"
          )
            ? "2"
            : "1",
        });
      }
    });

    console.log(arrayOfServices1);
    setarrayOfServices(arrayOfServices1);
  };

  const GetDoctorList = () => {
    let headers = getAuthHeader();
   
    axios
      .get<any>(endpoints.hostName + apiPath.DoctorsListApiPath, {headers}) //"http://34.131.82.122:3001/alemadi/mobile/doctor/list")
      .then((response) => {
        console.log(response.data);
        PopulateServices(response.data.departments);
      },
      (error) => {
        console.log("[Find Doctor page]: Error Fetching");
        presentToastWithOptions('Something went wrong','error');
        handleAuthError(error,history);

      }
      );
  };
  useEffect(() => {
    GetDoctorList();
  }, [searchText]);

  let strings = new LocalizedStrings({
    en: {
      findDoctor: "Find Doctor",
      selectLocation: "Select Location",
      alHilalWes: "Al Hilal WES",
      northARRayyan: "North, AR-RAYYAN",
      searchSpeciality: "Search Speciality",
    },
    ar: {
      findDoctor: "مستشفى",
      selectLocation: "مستشفى",
      alHilalWes: "مستشفى",
      northARRayyan: "مستشفى",
      searchSpeciality: "مستشفى",
    },
  });

  return (
    <IonPage>
      <Header title={strings.findDoctor} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="grid">
          <IonRow>
            <IonCol size="2" class="locationCol">
              <img className="location" src={location} alt="icon missing" style={{height:"250px"}}></img>
            </IonCol>
            <IonCol>
              <IonLabel class="selectlocation">
                {strings.selectLocation}
              </IonLabel>
              <br></br>
              <IonRadioGroup
                value={Hospitallocation}
                onIonChange={(e) => setHospitallocation(e.detail.value)}
              >
                <IonCol class="radioStyle" className="radiobutton">
                  <IonRadio value="2" class="radioposition" />
                  <IonLabel>
                    {" "}
                    <b>{strings.alHilalWes}</b>
                  </IonLabel>
                </IonCol>
                <IonRow>
                  <IonCol class="radioStyle" className="radiobutton">
                    <IonRadio value="1" class="radioposition" />
                    <IonLabel>
                      <b> {strings.northARRayyan}</b>
                    </IonLabel>
                  </IonCol>
                </IonRow>
              </IonRadioGroup>
            </IonCol>
          </IonRow>
          <br />

          <IonRow class="divider">
            <IonSearchbar
              value={searchText}
              onIonChange={(e) => setSearchText(e.detail.value!)}
              placeholder={strings.searchSpeciality}
              animated
              class="searchBar"
              debounce={500}
            ></IonSearchbar>
          </IonRow>
          <IonRow>
            {arrayOfServices
              .filter(
                (i) =>
                  i.department
                    .toLocaleLowerCase()
                    .includes(searchText.toLocaleLowerCase()) &&
                  i.location === Hospitallocation
              )
              .map((item) => (
                <IonCol key={item.icon} size="4" class="colContent">
                  <a href={item.url + "/" + item.department}>
                    <img src={item.icon} alt="icon is missing" style={{filter:"invert(30%)"}}></img>
                  </a>
                </IonCol>
              ))}
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default FindDoctor;
function PopulateServices(departments: any) {
  throw new Error("Function not implemented.");
}

function GetDoctorList() {
  throw new Error("Function not implemented.");
}

import React, { useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCheckbox,
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";
import { useHistory } from "react-router";
import axios from "axios";
import { endpoints, apiPath } from "../shared/application_constants";
import { presentToastWithOptions } from "../shared/toast/toast";
import LocalizedStrings from 'react-localization';
import { download } from "ionicons/icons";
import { Browser } from '@capacitor/browser';
const RegistrationStep4: React.FC = () => {


  const [TOC, setTOC] = useState();
  const [numPages, setNumPages] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  const [scaleFactor, setScaleFactor] = useState(0.75);

  const pdfURL=endpoints.hostName+"/pdf/TermsAndCond.pdf"

  

  // function onDocumentLoadSuccess({ numPages }: { numPages: any }) {
  //     console.log("PdfViewerChild setting page number:", numPages);

  //     setNumPages(numPages);
  //     setPageNumber(1);
  // }

  // function changePage(offset: any) {
  //     setPageNumber((prevPageNumber) => prevPageNumber + offset);
  // }

  // function previousPage() {
  //     changePage(-1);
  // }

  // function openPDF() {

  //   alert("PDF Opened");

  //       if(Browser) {
  //         Browser.open({ url: pdfURL});
  //       }else {
  //         history.push({
  //         pathname: "/PdfViewerParent",
  //         state: {
  //           pdfURL: pdfURL,
  //           btnName: "Close",
  //         },
  //       });
  //       }

  // }
  // function nextPage() {
  //     changePage(1);
  // }


  // function zoomOut(){
  //   if(scaleFactor>0.75){
  //     setScaleFactor(scaleFactor-0.1)
  //   }
  // }

  // function zoomIn(){
    
  //     setScaleFactor(scaleFactor+0.1)
 
  // }

  // const DownloadButton = () => {
  //     //   saveAs(URL, "Alemadi.pdf");
  // };

  const [disableConfirmBtn, setdisableConfirmBtn] = useState(true);
  let history: any = useHistory();
  const CheckStatus = (event: any) => {
    if (event) setdisableConfirmBtn(false);
    else setdisableConfirmBtn(true);
  };

  const Toc = () => {
    axios.get(endpoints.hostName + apiPath.TocApiPath) //"http://34.131.82.122:3001/docs/toc.txt")
    .then((response) => {
      console.log(response.data);
      setTOC(response.data);
    }
    )
  };
  Toc();

  const registerpatient = () => {
    let data = history.location.state;
    console.log(
      "[Registration page step 4: register patient function]: data to upload to the server ",
      data
    );

    axios.post(endpoints.hostName + apiPath.registerUserApiPath, data).then(
      (response) => {
        if (response.status === 200) {
          console.log("[Registration step 4:registerpatient function] Patient registered");
          presentToastWithOptions("Registered Successfully.");
          history.push({ pathname: '/Login' })

        } else {
          console.log("[Registration step 4]: Registration failed");
          presentToastWithOptions("Registeration Failed.");
        }
      },
      (error) => {
        console.log("[Registration step 4]: Registration failed");
        presentToastWithOptions("Registeration Failed.");
      }
    );
  };



  let strings = new LocalizedStrings({
    en: {
      registration: "Registration",
      step4: "STEP-4",
      agreeToTermsAndConditions: "Agree to terms and conditions",
      submit: "Submit"
    },
    ar: {
      registration: "مستشفى",
      step4: "مستشفى",
      agreeToTermsAndConditions: "مستشفى",
      submit: "مستشفى",
    }
  }
  );

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="headergrid">
          <br />
          <IonRow>
            <IonBackButton
              defaultHref="RegistrationStep3"
              color="light"
              class="backbutton"
            />
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>{strings.registration}</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card">
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>{strings.step4}</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div >
                    {/* pdf block */}


                    {/* <IonRow>
                      <IonCol size="12">
                     


                      <div className={"registration_pdf"} onClick={openPDF}>
                    <div className="pdf_toolbar">
                        <div className="navigation_button_container">

                            <div
                                onClick={previousPage}
                                className={
                                    "material-icons navigation_icon" +
                                    (pageNumber <= 1 ? " disabled" : "")
                                }
                            >
                                arrow_back
                            </div>
                            <div>
                                {pageNumber || (numPages ? 1 : "--")} of {numPages || "--"}
                            </div>
                            <div
                                onClick={nextPage}
                                className={
                                    "material-icons navigation_icon" +
                                    (pageNumber >= numPages ? " disabled" : "")
                                }
                            >
                                arrow_forward
                            </div>
                        </div>
                    </div>
                    <Document file={pdfURL} onLoadSuccess={onDocumentLoadSuccess}>
                        {/* <Page scale={0.5} pageNumber={pageNumber} /> */}
                        {/* <Page scale={scaleFactor}   pageNumber={pageNumber} />
                    </Document>
                    <div style={{background:"black", display:"flex",flexDirection:"row",justifyContent:"space-around"}}>
                    <div
                                onClick={zoomOut}
                                className={
                                    "material-icons navigation_icon"   
                                }
                            >
                                zoom_out
                            </div>
                            <div
                                onClick={zoomIn}
                                className={
                                    "material-icons navigation_icon" 
                                }
                            >
                                zoom_in
                            </div>
                    </div>
                </div> */}


                {/* <div style={{color:"white",margin:"5px 5px 5px 0px"}}>
                  <a className={"toc"} style={{color:"whtie"}} onClick={openPDF}>Open Terms & Condition</a>
                </div>
                      </IonCol>

                    </IonRow> */} 

                    {/* pdf block ends here */}
                    <IonCard className="TOC" >
                     <IonCardContent>
                        <p>{TOC}</p>
                  </IonCardContent>
                    </IonCard>
                    <IonRow>
                      <IonCol size="2">
                        <IonCheckbox
                          class="checkBox"
                          color="primary"
                          onIonChange={(e) => CheckStatus(e.detail.checked)}
                        />{" "}
                        {"     "}
                      </IonCol>
                      <IonCol size="10" class="textCol">
                        <IonText class="textStyle">
                          {strings.agreeToTermsAndConditions}
                        </IonText>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        // routerLink="/Login"
                        class="nextBtn"
                        strong={true}
                        onClick={(e) => registerpatient()}
                        disabled={disableConfirmBtn}
                      >
                        {strings.submit}
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep4;

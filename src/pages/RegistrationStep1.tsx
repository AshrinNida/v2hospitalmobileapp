import React, { useState, useRef } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";
import "./Login.css";
import { useHistory } from "react-router-dom";
import LocalizedStrings from "react-localization";

const RegistrationStep1: React.FC = () => {
  const history = useHistory();
  const qidRef = useRef<HTMLIonInputElement>(null);
  const MobNoRef = useRef<HTMLIonInputElement>(null);
  const [QIDerror, setQIDError] = useState<string>("");
  const [MobNumerror, setMobNumError] = useState<string>("");


 /* let strings = new LocalizedStrings({
    en: {
      registration: "Registration",
      step1: "Step 1",
      enterQIDNumber: "Enter QID Number",
      enterMobileNumber: "Enter Mobile Number",
      next: "Next",
    },
    ar: {
      registration: "مستشفى",
      step1: "مستشفى",
      enterQIDNumber: "مستشفى",
      enterMobileNumber: "مستشفى",
      next: "مستشفى",
    },
  });*/

  //#region Input Validation and button enable/disable
  const numberOnlyValidation = (event: any) => {
    const pattern = /[0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  };

  const QIdValidation = (event: any) => {
    const pattern2 = /[0-9.,]/;
    const pattern3 = /^[2-3][0-9.,]/;
    let inputChar = String.fromCharCode(event.charCode);
    console.log(pattern3.test(event.target.value));
    if (!event.target.value || event.target.value == "") {
      if (inputChar != "2" && inputChar != "3") {
        event.preventDefault();
        setQIDError("QID starts from 2 or 3");
      } else {
        setQIDError("");
      }
    } else if (!pattern2.test(inputChar)) {
      event.preventDefault();
      setQIDError("QID should be a number");
    } else {
      setQIDError("");
    }
  };


  const ValidateQIDBeforeSubmit = (value:any) => {
      const pattern = /^[2-3][0-9]{10}$/gm;
      
      if(!pattern.test(value)) {
        setQIDError("Invalid QID");
        return false;
      }else {
        setQIDError(" ");
        return true;
      }
    };

  const ValidatePhoneBeforeSubmit = (value:any) => {
      const pattern = /^[0-9]{8}$/gm;
      
      if(!pattern.test(value)) {
        setMobNumError("Invalid Mobile Number");
        return false;
      }else {
        setMobNumError(" ");
        return true;
      }
    };

  const QIDValidationForLength = (event: any) => {
    if (event.target.value.length > 0 && event.target.value.length != 11) {
      setQIDError("Invalid QID");
    } else {
      setQIDError("");
    }
  };

  let strings = new LocalizedStrings({
    en:{
      registration:"Registration",
      step1:"Step 1",
      enterQIDNumber:"Enter QID Number",
      enterMobileNumber:"Enter Mobile Number",
      next:"Next",
  
    },
    ar: {
      registration:"مستشفى",
      step1:"مستشفى",
      enterQIDNumber:"مستشفى",
      enterMobileNumber:"مستشفى",
      next:"مستشفى",
    }
  }
  );
  const MobileNumberOnFocus = (event: any) => {
     setMobNumError(" ");
  };
  const MobileNumberValidationForLength = (event: any) => {
    if (event.target.value.length > 0 && event.target.value.length != 8) {
      setMobNumError("Invalid Mobile number");
      return false;
    } else {
      setMobNumError(" ");
    }
  };
  const ValidateFields = () => {
    if (
      qidRef.current!.value != "" &&
      MobNoRef.current!.value != ""
    ) {

      if(ValidateQIDBeforeSubmit(qidRef.current!.value) && ValidatePhoneBeforeSubmit(MobNoRef.current!.value)){

        history.push({ pathname: "/RegistrationStep2", state: getUserData() });  
      }
      
    } else {
      if (qidRef.current!.value != "") {
        setQIDError("");
      } else {
        setQIDError("This field is Required");
        return false;
      }

      if (MobNoRef.current!.value != "") {
        setMobNumError("");
      } else {
        setMobNumError("This field is Required");
        return false;
      }
    }
  };
  //#endregion

  //#region Business Logic

  const getUserData = () => {
    console.log("[Registration step 1: inside getUserDate funtion]");

    return {
      qatarIdInputRef: qidRef.current!.value!,
      phoneNumberInputRef: MobNoRef.current!.value!,
    };
  };
  //#endregion

  

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="headergrid">
          <br/>
          <IonRow>
            <IonBackButton
              defaultHref="Login"
              color="light"
              class="backbutton"
            />{" "}
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>{strings.registration}</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="card" style={{height:"330px"}}>
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>{strings.step1}</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          {strings.enterQIDNumber}
                        </IonLabel>
                        <IonInput
                          placeholder={strings.enterQIDNumber}
                          clear-input={true}
                          autofocus={true}
                          type="tel"
                          maxlength={11}
                          onKeyPress={QIdValidation}
                          ref={qidRef}
                          onBlur={QIDValidationForLength}
                        ></IonInput>
                        {QIDerror && (
                          <IonText color="danger">
                            <p>{QIDerror}</p>
                          </IonText>
                        )}
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          {strings.enterMobileNumber}
                        </IonLabel>
                        <IonInput
                          placeholder={strings.enterMobileNumber}
                          clear-input={true}
                          autofocus={true}
                          type="tel"
                          maxlength={8}
                          ref={MobNoRef}
                          onKeyPress={numberOnlyValidation}
                          onBlur={MobileNumberValidationForLength}
                          onFocus={MobileNumberOnFocus}
                        ></IonInput>
                        {MobNumerror && (
                          <IonText color="danger">
                            <p>{MobNumerror}</p>
                          </IonText>
                        )}
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        class="nextBtn"
                        strong={true}
                        onClick={ValidateFields}
                      >
                        {strings.next}
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep1;



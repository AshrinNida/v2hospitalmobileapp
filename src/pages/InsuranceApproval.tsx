import {
  IonBadge,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonLoading,
  IonPage,
  IonRow,
} from "@ionic/react";
import Footer from "./Footer";
import Header from "./Header";
import "./InsuranceApproval.css";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import LocalizedStrings from "react-localization";
import { useEffect, useState } from "react";
import axios from "axios";
import { apiPath, endpoints, testData } from "../shared/application_constants";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import { presentToastWithOptions } from "../shared/toast/toast";
import { useHistory } from "react-router";

const InsuranceApproval: React.FC = () => {
  interface Idetails {
    name: string;
    details: Iorderdetails[];
  }

  interface Iorderdetails {
    code: string;
    desc: string;
    status: string;
  }

  const [labReports, setLabReports] = useState<any[]>([]);
  const [MRIReports, setMRIReports] = useState<any[]>([]);
  const [SurgeryReports, setSurgeryReports] = useState<any[]>([]);
  const [showLoadingAnimation, setShowLoadingAnimation] = useState(true);
  let history: any = useHistory();


  var colorStatus = function(status: string) {
    if(status === 'Approved') {
      return 'ion-color ion-color-success md hydrated';
    } else if (status === 'In Progress') {
        return 'ion-color ion-color-warning md hydrated';
    } else {
      return 'ion-color ion-color-danger md hydrated';
    }
  }

  let strings = new LocalizedStrings({
    en: {
      insuranceApprovalpage: "Insurance Approval page",
      approved: " Approved",
      rejected: " Rejected",
      ordCode: "ORDCODE",
      code: "Code",
      desc: "DESC",
      orderLab: "Order LAB",
    },
    ar: {
      insuranceApprovalpage: "مستشفى",
      approved: "مستشفى",
      rejected: "مستشفى",
      ordCode: "مستشفى",
      code: "مستشفى",
      desc: "مستشفى",
      orderLab: "مستشفى",
    },
  });

  useEffect(() => {
    const getPatientInsuranceDetailsUsingPatId = () => {
      let headers = getAuthHeader();
     
      axios
        .get(endpoints.hostName + apiPath.insuranceApiPath + testData.patCode, { headers })
        .then((data: any) => {
          if (data && data.status == 200) {
            if (data && data.status == 200) {
              console.log(
                "[Lab reports: getPatientReportsUsingPatId function] data from server: ",
                data
              );

              if (data.data) {
                setLabReports(data.data["Lab"]);
                setMRIReports(data.data["MRI"]);
                setSurgeryReports(data.data["Surgery"]);
              }
            } else {
              // Todo handle error here
            }
          } else {
            // Todo handle error here
          }
        })
        .catch((error) => {
          // Todo handle error here
          presentToastWithOptions('Something went wrong','error');
          handleAuthError(error,history);


          alert(error);
        })
        .finally(() => {
          // close the loading animation here
          setShowLoadingAnimation(false);
        });
    };
    getPatientInsuranceDetailsUsingPatId();
  }, []);

  return (
    <IonPage>
      <Header title={strings.insuranceApprovalpage} defaultRoute={"/Main"} />
      <IonContent fullscreen className="ion-padding">
        {labReports && (
          <Accordion allowZeroExpanded={true}>
            <AccordionItem>
              <AccordionItemHeading id="Toggle" className="fontColor">
                <AccordionItemButton
                  style={{ backgroundColor: "#5a62ad", color: "white" }}
                >
                  Lab
                </AccordionItemButton>
              </AccordionItemHeading>
              <IonGrid class="insurancegrid">
                {labReports.map((report: any, i) => (
                  <AccordionItemPanel key={i} className="accordianPanel">
                    <IonCard className="fontColor">
                      <IonCardContent class="color">
                        <IonRow>
                          <IonCol size="5">ID: {report.ID}</IonCol>
                          <IonCol size="7">Desc: {report.DESC}</IonCol>
                        </IonRow>
                        <IonRow>
                          <IonCol size="5">Date: {report.DATE}</IonCol>
                          <IonCol size="7">
                            <span>
                              <IonBadge className={colorStatus(report.STATUS)}>
                                {report.STATUS}
                              </IonBadge>
                            </span>
                          </IonCol>
                        </IonRow>
                      </IonCardContent>
                    </IonCard>
                  </AccordionItemPanel>
                ))}
                <br />
                {MRIReports && (
                  <AccordionItem>
                    <AccordionItemHeading id="Toggle">
                      <AccordionItemButton
                        style={{ backgroundColor: "#5a62ad", color: "white" }}
                      >
                        MRI
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <IonGrid class="insurancegrid">
                      {MRIReports.map((report: any, i) => (
                        <AccordionItemPanel key={i} className="accordianPanel">
                          <IonCard className="fontColor">
                            <IonCardContent class="color">
                              <IonRow>
                                <IonCol size="5">ID: {report.ID}</IonCol>
                                <IonCol size="7">Desc: {report.DESC}</IonCol>
                              </IonRow>
                              <IonRow>
                                <IonCol size="5">Date: {report.DATE}</IonCol>
                                <IonCol size="7">
                                  <IonBadge className={colorStatus(report.STATUS)}>
                                    {report.STATUS}
                                  </IonBadge>
                                </IonCol>
                              </IonRow>
                            </IonCardContent>
                          </IonCard>
                        </AccordionItemPanel>
                      ))}
                    </IonGrid>
                  </AccordionItem>
                )}

                <br />
                {SurgeryReports && (
                  <AccordionItem>
                    <AccordionItemHeading id="Toggle">
                      <AccordionItemButton
                        style={{ backgroundColor: "#5a62ad", color: "white" }}
                      >
                        Surgery
                      </AccordionItemButton>
                    </AccordionItemHeading>
                    <IonGrid class="insurancegrid">
                      {SurgeryReports.map((report: any, i) => (
                        <AccordionItemPanel key={i} className="accordianPanel">
                          <IonCard className="fontColor">
                            <IonCardContent class="color">
                              <IonRow>
                                <IonCol size="5">ID: {report.ID}</IonCol>
                                <IonCol size="7">Desc: {report.DESC}</IonCol>
                              </IonRow>
                              <IonRow>
                                <IonCol size="5">Date: {report.DATE}</IonCol>
                                <IonCol size="7">
                                  <IonBadge className={colorStatus(report.STATUS)}>
                                    {report.STATUS}
                                  </IonBadge>
                                </IonCol>
                              </IonRow>
                            </IonCardContent>
                          </IonCard>
                        </AccordionItemPanel>
                      ))}
                    </IonGrid>
                  </AccordionItem>
                )}
              </IonGrid>
            </AccordionItem>
          </Accordion>
        )}{" "}
        <IonLoading
          isOpen={showLoadingAnimation}
          // onDidDismiss={() => setShowLoadingAnimation(false)}
          message={"Loading..."}
        />
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default InsuranceApproval;

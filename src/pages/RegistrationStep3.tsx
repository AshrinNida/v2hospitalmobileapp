import React, { useRef, useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonLabel,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonText,
  IonIcon,
  IonInput,
  withIonLifeCycle, 
  useIonViewWillEnter
} from "@ionic/react";
import "./Registration.css";
import { camera } from "ionicons/icons";
import { useHistory } from "react-router";
import { Plugins } from "@capacitor/core";
import { CameraResultType, CameraSource } from "@capacitor/camera";
import LocalizedStrings from "react-localization";
import axios from "axios";
import { endpoints, apiPath } from "../shared/application_constants";

const { Camera } = Plugins;

const RegistrationStep3: React.FC = () => {
  const labelTxt = "No photo selected";

  // const [disableLoginBtn, setdisableLoginBtn] = useState(true);
  const [takenPhotoFront, setTakenPhotoFront] =
    useState<{ path: string; filename: any }>();
  const [takenPhotoBack, setTakenPhotoBack] =
    useState<{ path: string; filename: any }>();
  const [takenPhotoInsurance, setTakenPhotoInsurance] =
    useState<{ path: string; filename: any }>();

  const [insuranceCompanyList, setInsuranceCompanyList]= useState([]);
  const [selectedInsuranceCompany, setSelectedInsuranceCompany]= useState('');

    const [qIdfrontImageBase64, setQIdfrontImageBase64] = useState('');
    const [qIdbackImageBase64, setQIdbackImageBase64] = useState('');
    const [insuranceImageBase64, setInsuranceImageBase64] = useState('');
  // Upload QID Front
  const takePhotoFront = async () => {
    var photo;
    try {
      photo = await Camera.getPhoto({
        resultType: CameraResultType.Uri,
        source: CameraSource.Prompt, //(Prompt-Prompts the user to select either the photo album or take a photo)
        allowEditing: false,
        quality: 80,
      });
      if (!photo || !photo.path || !photo.webPath) {
        return;
      }
    } catch (err) {
      console.log(err);
    }

    var x = photo.path.split("/");

    setTakenPhotoFront({
      path: photo.path,
      filename: x.pop(),
    });
    // qIdfrontImageBase64=getImageString(photo)
    getBase64(photo?.webPath, (data: any) => {
      console.log('Registration page step 3, Printing base64 ',data);
      
      // qIdfrontImageBase64 =   data
      setQIdfrontImageBase64(data)
    });
    
    console.log(photo);
  };

  useIonViewWillEnter(() => {
    getInsuranceCompanyList()
  });


  const getInsuranceCompanyList =()=>{
    axios
    .get(
      endpoints.hostName +
        apiPath.insuranceListApiPath
    )
    .then((response:any) => {
      if (response && response.data&&response.data.length>0) {
        console.log("[Registration page step 3: getInsuranceCompanyList: data from server ]",response.data);
        setInsuranceCompanyList(response.data)
      
      }
    },(error) => {
      console.error("[Registration page step 3: getInsuranceCompanyList: error while fetching data from server ]",error);
    });
  }



  // Upload QID Back
  const takePhotoBack = async () => {
    const photo:any = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Prompt,
      quality: 80,
      width: 200,
    });
    if (!photo || !photo.path || !photo.webPath) {
      return;
    }
    var x = photo.path.split("/");
    setTakenPhotoBack({
      path: photo.path,
      filename: x.pop(),
    });
    console.log(photo);
    // qIdbackImageBase64=getImageString(photo)
    getBase64(photo?.webPath, (data: any) => {
      console.log('Registration page step 3, Printing base64 ',data);
      
      setQIdbackImageBase64(data)
    });
  };

  // Upload Insurance
  const takePhotoInsurance = async () => {
    const photo:any = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Prompt,
      quality: 80,
      width: 200,
    });
    if (!photo || !photo.path || !photo.webPath) {
      return;
    }
    var x = photo.path.split("/");
    setTakenPhotoInsurance({
      path: photo.path,
      filename: x.pop(),
    });
    getBase64(photo?.webPath, (data: any) => {
      console.log('Registration page step 3, Printing base64 ',data);
      
  
      setInsuranceImageBase64(data)
    });
    console.log(photo);
  };

  //--> Validations

  const Validate = () => {
    if (
      takenPhotoFront &&
      takenPhotoBack &&
      takenPhotoInsurance &&
      selectedCompany != null
    ) {
      // history.push("/RegistrationStep4");
      history.push({ pathname: "/RegistrationStep4", state: getUserData() });
    }
    return false;
  };

  //Validations-End <--

  const fileInput: any = useRef(null);
  let history: any = useHistory();

  let selectedCompany: String = "";
  // console.log('[registration slide 2]: printing details from step 1',history.location.state.qatarIdInputRef);

  // Data to forward to the next page
  const getUserData = () => {
    return {
      QID: history.location.state.qatarIdInputRef,
      FNAME: history.location.state.firstNameInputRef,
      LNAME: history.location.state.lastNameInputRef,
      MOBILE: history.location.state.phoneNumberInputRef,
      QIDDOCF: qIdfrontImageBase64,
      QIDDOCB: qIdbackImageBase64,
      INSURANCEDOC: insuranceImageBase64,
      POLICYNO:selectedInsuranceCompany
    };
  };

  const captureCompanySelection = (selection: any) => {
    let selectedCompany = selection.detail.value;
    setSelectedInsuranceCompany(selectedCompany)
  };

  // function to convert selected image to base64 string
  // const getBase64 = (file: File, cb: Function) => {
  //   console.log("[Registration step 3 getBase64 function ] ");

  //   let reader = new FileReader();
  //   reader.readAsDataURL(file);
  //   reader.onload = function () {
  //     cb(reader.result);
  //   };
  //   reader.onerror = function (error) {
  //     console.log(
  //       "[Registration step 3 getBase64 function ]Error while converting iamge to base64: ",
  //       error
  //     );
  //   };
  // };


  function getBase64(url:string, callback:any) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      var reader = new FileReader();
      reader.onloadend = function() {
        console.log('[registration page step 3: getBase64 function]',reader.result);
        
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  let strings = new LocalizedStrings({
    en: {
      registration: "Registration",
      step3: "Step 3",
      uploadQID: "Upload QID",
      front: "Front",
      back: "Back",
      insuranceCompany: "Insurance Company",
      sevenService: "Seven Service",

      alKoot: "Al Koot",
      metLifeAlico: " Metlife(Alico)",
      almadallah: "Almadallah",
      allinaz: "Allinaz",
      amityHealthComprehensiveNetworkOnly:
        "Amity Health(Comprehensive Network Only)",
      aspetar: "Aspetar",
      axaInsuranceGulfBSCC: "Axa Insurance (GULF) B.S.C (C) ",
      bupa: "Bupa",
      cigna: "Cigna",
      crownTownsNoorCare: "CrownTowns - Noor Care ",
      crownTownsNoorCareNorthGate: "crownTowns - Noor Care (North Gate) ",
      damanUAE: "Daman UAE ",
      damanQatar: "Daman Qata",
      engineeringConsultantGroup: "Engineering Consultant Group ",
      engineeringConsultantGroupNorthGate:
        "Engineering Consultant Group(North Gate)",
      embassyOfTheStateOfKuwait: "Embassy Of The State Of Kuwait",
      GemsCOWLL: "Gems CO.W.L.L ",
      geoBlue: "Geo Blue",
      interglobal: "Intergloba",
      gmcServicesHenner: "GMC Services / Henner ",
      generalRetirementAndSOocialInsurance:
        "GeneralL Retirement And Social Insurance ",
      health360: "Health 360",
      inayah: "Inayah",
      itt: "ITT ",
      globemed: "Globemed",
      minorsAffairsAuthorityNorthGate:
        "Minors Affairs Authority ( North Gate )",
      mednetBahrain: "Mednet Bahrain ",
      meddy: "Meddy",
      ministryOfMunicipalityAndEnvironment:
        " Ministry Of Municipality And Environment",
      moore: "Moore",
      mooreNorthGate: "Moore (North Gate) ",
      mobilitySaintHonoreMSHInternation:
        "Mobility Saint Honore (MSH) Internation",
      muntajat: "Muntajat",
      nas: "NAS",
      nestle: "Nestle",
      neuron: "Neuron",
      nextCare: "Next Care",
      sevenServicesNG: "Seven Services - NG ",
      northBranchAntiDopingLabQatar: "North Branch Anti Doping Lab Qatar",
      northBranchAetna: "North Branch Aetna",
      northBranchAlKoot: "North Branch Al Koot",
      northBranchMetlifeAlico: "North Branch Metlife (ALICO) ",
      northBranchAlmadallah: "North Branch Almadallah",
      northBranchAllianz: "North Branch Allianz",
      northBranchAmityHealthComprehensive:
        "North Branch Amity Health Comprehensive",
      northBranchAspetar: "North Branch Aspetar",
      northBranchAXAInsuranceGulfBSC:
        "North Branch AXA Insurance (GULF) B.S.C ",
      northBranchBupa: "North Branch Bupa",
      northBranchCigna: "North Branch Cigna",
      northBranchDawanUAE: "North Branch Dawan UAE",
      northBranchDamanQatar: "North Branch Dawan Qatar",
      northBranchFMC: "North Branch FMC",
      northBranchGemsCOWLL: "North Branch Gems CO. W.L.L ",
      northBranchGEOBlue: "North Branch GEO Blue",
      northBranchInterglobal: "North Branch Interglobal",
      northBranchGMCServicesHenner: "North Branch GMC Services / HENNER ",
      northGeneralRetirementAndSocialINS: "North General Retirement Social INS",
      northBranchHealth360: "North Branch Health 360",
      northBranchInayah: "North Branch Inayah",
      northBranchITT: "North Branch ITT",
      northBranchGlobemed: "North Branch Globemed",
      northBranchMednetBahrain: "North Branch Mednet Bahrain ",
      ngMeddy: "NG Medy ",
      northBranchMobilitySaintHonoreMSH:
        "North Branch Mobility Saint Honore (MSH)",
      northBranchMuntajat: "North Branch Muntajat",
      northBranchNAS: "North Branch NAS",
      northBranchNestle: "North Branch Nestle",
      northBranchNeuron: "North Branch Neuron",
      northBranchNextcare: "North Branch Nextcare",
      northBranchNationalHealthInsuranceC:
        "North Branch National Health Insurance C",
      northBranchNOWHealthInternational:
        "North Branch NOW Health International",
      northBranchOmanInsuranceCompany: "North Branch OMAN Insurance Company ",
      northBranchQatarFinancialCenter: "North Branch Qatar Financial Center",
      northBranchQICAnaya: "North Branch QIC-ANAYA ",
      northBranchQLMQLifeAndMedicalInsurance:
        "NNorth Branch QLM (Q LIFE & MEDICAL INSURANCE)",
      qatarTriathlonFederationNorthGate:
        "Qatar Triathlon Federation ( North Gate)",
      northBranchSaudiArabianInsuranceCOM:
        "North Branch Saudi Arabian Insurance COM",
      supremeCommitteeOfJusticeNorthGate:
        "Supreme Committee Of Justice (North Gate)",
      northBranchInternationalSOS: "North Branch International SOS ",
      northBranchTasweeq: "North Branch Tasweeq",
      testaahel2020NorthGate: "testaahel 2020 (North Gate) ",
      northBranchMednetUAEGoldNetworkOn:
        "North Branch Mednet UAE (GOLD NETWORK ON)",
      northBranchWaped: "North Branch Wapped",
      nationalHealthInsuranceCompanyNHIC:
        "National Health Insurance Company (NHIC)",
      nowHealthInternational: "NOW Health International ",
      omanInsuranceCompany: "OMAN Insurance Company ",
      qatarFinancialCenter: "Qatar Financial Center",
      qicAnaya: "QIC-ANAYA ",
      qlmQLifeAndMedicalInsuranceCompany:
        "QLM (Q LIFE & MEDICAL INSURANCE COMPANY)",
      beemaQLMQLifeAndMedicalInsuranceCO:
        "BEEMA-QLM (Q LIFE & MEDICAL INSURANCE CO)",
      qatarMuseumAuthority: "Qatar Museum Authority ",
      qatarMuseumAuthorityNorthGate: "Qatar Museum Authority ( North Gate )",
      qatarTriathlonFederation: "Qatar Trithlon Federation",
      qatarTableTennisAssociationNG: "Qatar Table Tennis Association-NG ",
      saudiArabianInsuranceCompanySAICO:
        "Saudi Arabian Insurance Company (SAICO)",
      supremCommitteeOfJustice: "Suprem Committee Of Justice  ",
      shaikhJassimPakage: "Shaikh Jassim Pakage",
      shaikhJassimKhalifaALThaniPKGNG:
        "Shaikh Jassim Khalifa AL THANI - PKG NG",
      internationalSOS: "International SOS",
      tasweeq: "Tasweeq",
      testaahel2020MinitryOfDefence: "Testaahel 2020 (MINISTRY OF DEFENCE) ",
      mednetUAEGoldNetworkOnly: "Mednet UAE (GOLD NETWORK ONLY)",
      wapmed: "Wapmed ",
      next: "Next",
    },
    ar: {
      registration: "مستشفى",
      step3: "مستشفى",
      uploadQID: "مستشفى",
      front: "مستشفى",
      back: "مستشفى",
      insuranceCompany: "مستشفى",
      sevenService: "مستشفى",
      antiDopingLabQatar: "مستشفى",
      aetna: "مستشفى",
      alKoot: "مستشفى",
      metLifeAlico: "مستشفى",
      almadallah: "مستشفى",
      allinaz: "مستشفى",
      amityHealthComprehensiveNetworkOnly: "مستشفى",
      aspetar: "مستشفى",
      axaInsuranceGulfBSCC: "مستشفى",
      bupa: "مستشفى",
      cigna: "مستشفى",
      crownTownsNoorCare: "مستشفى",
      crownTownsNoorCareNorthGate: "مستشفى",
      damanUAE: "مستشفى",
      damanQatar: "مستشفى",
      engineeringConsultantGroup: "مستشفى",
      engineeringConsultantGroupNorthGate: "مستشفى",
      embassyOfTheStateOfKuwait: "مستشفى",
      gemsCOWLL: "مستشفى",
      geoBlue: "مستشفى",
      interglobal: "مستشفى",
      gmcServicesHenner: "مستشفى",
      generalRetirementAndSOocialInsurance: "مستشفى",
      health360: "مستشفى",
      inayah: "مستشفى",
      itt: "مستشفى",
      globemed: "مستشفى",
      minorsAffairsAuthority: "مستشفى",
      minorsAffairsAuthorityNorthGate: "مستشفى",
      mednetBahrain: "مستشفى",
      meddy: "مستشفى",
      ministryOfMunicipalityAndEnvironment: "مستشفى",
      moore: "مستشفى",
      mooreNorthGate: "مستشفى",
      mobilitySaintHonoreMSHInternation: "مستشفى",
      muntajat: "مستشفى",
      nas: "مستشفى",
      nestle: "مستشفى",
      neuron: "مستشفى",
      nextCare: "مستشفى",
      sevenServicesNG: "مستشفى",
      northBranchAntiDopingLabQatar: "مستشفى",
      northBranchAetna: "مستشفى",
      northBranchAlKoot: "مستشفى",
      northBranchMetlifeAlico: "مستشفى",
      northBranchAlmadallah: "مستشفى",
      northBranchAllianz: "مستشفى",
      northBranchAmityHealthComprehensive: "مستشفى",
      northBranchAspetar: "مستشفى",
      northBranchAXAInsuranceGulfBSC: "مستشفى",
      northBranchBupa: "مستشفى",
      northBranchDawanUAE: "مستشفى",
      northBranchDamanQatar: "مستشفى",
      northBranchFMC: "مستشفى",
      northBranchGemsCOWLL: "مستشفى",
      northBranchGEOBlue: "مستشفى",
      northBranchInterglobal: "مستشفى",
      northBranchGMCServicesHenner: "مستشفى",
      northGeneralRetirementAndSocialINS: "مستشفى",
      northBranchHealth360: "مستشفى",
      northBranchInayah: "مستشفى",
      northBranchITT: "مستشفى",
      northBranchGlobemed: "مستشفى",
      northBranchMednetBahrain: "مستشفى",
      ngMeddy: "مستشفى",
      northBranchMobilitySaintHonoreMSH: "مستشفى",
      northBranchMuntajat: "مستشفى",
      northBranchNAS: "مستشفى",
      northBranchNestle: "مستشفى",
      northBranchNeuron: "مستشفى",
      northBranchNextcare: "مستشفى",
      northBranchNationalHealthInsuranceC: "مستشفى",
      northBranchNOWHealthInternational: "مستشفى",
      northBranchOmanInsuranceCompany: "مستشفى",
      northBranchQatarFinancialCenter: "مستشفى",
      northBranchQICAnaya: "مستشفى",
      northBranchQLMQLifeAndMedicalInsurance: "مستشفى",
      qatarTriathlonFederationNorthGate: "مستشفى",
      northBranchSaudiArabianInsuranceCOM: "مستشفى",
      supremeCommitteeOfJusticeNorthGate: "مستشفى",
      northBranchInternationalSOS: "مستشفى",
      northBranchTasweeq: "مستشفى",
      testaahel2020NorthGate: "مستشفى",
      northBranchMednetUAEGoldNetworkOn: "مستشفى",
      northBranchWaped: "مستشفى",
      nationalHealthInsuranceCompanyNHIC: "مستشفى",
      nowHealthInternational: "مستشفى",
      omanInsuranceCompany: "مستشفى",
      qatarFinancialCenter: "مستشفى",
      qicAnaya: "مستشفى",
      qlmQLifeAndMedicalInsuranceCompany: "مستشفى",
      beemaQLMQLifeAndMedicalInsuranceCO: "مستشفى",
      qatarMuseumAuthority: "مستشفى",
      qatarMuseumAuthorityNorthGate: "مستشفى",
      qatarTriathlonFederation: "مستشفى",
      qatarTableTennisAssociationNG: "مستشفى",
      saudiArabianInsuranceCompanySAICO: "مستشفى",
      supremCommitteeOfJustice: "مستشفى",
      shaikhJassimPakage: "مستشفى",
      shaikhJassimKhalifaALThaniPKGNG: "مستشفى",
      internationalSOS: "مستشفى",
      tasweeq: "مستشفى",
      testaahel2020MinitryOfDefence: "مستشفى",
      mednetUAEGoldNetworkOnly: "مستشفى",
      wapmed: "مستشفى",
      next: "مستشفى",
    },
  });

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="headergrid">
          <br />
          <IonRow>
            <IonBackButton
              defaultHref="RegistrationStep2"
              color="light"
              class="backbutton"
            />
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  <b>{strings.registration}</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card" style={{height:"500px"}}>
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>{strings.step3}</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent style={{overflow:"scroll"}}>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          {strings.uploadQID}
                        </IonLabel>
                        <IonInput
                          readonly
                          class="Input"
                        >
                          <IonButton
                            fill="solid"
                            onClick={takePhotoFront}
                            id="qIdfrontImageBase64"
                          >
                            <IonIcon icon={camera} slot="start" />
                            <IonLabel class="btn">front</IonLabel>
                          </IonButton>
                        </IonInput>
                        <div>
                          {!takenPhotoFront && (
                            <h3 style={{ color: "#f24949" }}> {labelTxt}</h3>
                          )}
                          {takenPhotoFront && (
                            <h4>{takenPhotoFront.filename}</h4>
                          )}
                        </div>
                        <IonInput
                          readonly
                          class="Input"
                        >
                          <IonButton
                            fill="solid"
                            onClick={takePhotoBack}
                            id="qIdbackImageBase64"
                          >
                            <IonIcon icon={camera} slot="start" />
                            <IonLabel class="btn">Back</IonLabel>
                          </IonButton>
                        </IonInput>
                        <div>
                          {!takenPhotoBack && (
                            <h3 style={{ color: "#f24949" }}>{labelTxt}</h3>
                          )}
                          {takenPhotoBack && <h4>{takenPhotoBack.filename}</h4>}
                        </div>
                        <IonInput
                        readonly
                        class="insuranceInput" 
                        className="input"
                      >
                        
                          <IonButton
                            fill="solid"
                            onClick={takePhotoInsurance}
                            id="insuranceImageBase64"
                          >
                            <IonIcon icon={camera} slot="start" />
                            <IonLabel class="btn">Insurance</IonLabel>
                          </IonButton>
                        </IonInput>
                        <div>
                          {!takenPhotoInsurance && (
                            <h3 style={{ color: "#f24949" }}>{labelTxt}</h3>
                          )}
                          {takenPhotoInsurance && (
                            <h4 className="file">
                              {takenPhotoInsurance.filename}
                            </h4>
                          )}
                        </div>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          {strings.insuranceCompany}
                        </IonLabel>
                        <IonSelect
                          class="multiselect"
                          className="dropdown"
                          placeholder="Select A Company"
                          onIonChange={(e) => captureCompanySelection(e)}>
                           {insuranceCompanyList.map((company:any) => (
                <IonSelectOption key={company.INSUCODE} value={company.INSUCODE}>
                        {company.NAME} 
                </IonSelectOption>
              ))}
                        </IonSelect>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        onClick={Validate}
                        class="nextBtn"
                        strong={true}
                      >
                        {strings.next}
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep3;

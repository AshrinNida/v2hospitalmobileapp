import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonImg,
  IonItem,
  IonLabel,
  IonModal,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useCallback, useEffect, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./BookAnAppointmentStep2.css";
import Doctor from "../images/Doctor-01.png";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import { useParams } from "react-router";
import "../../node_modules/@syncfusion/ej2-base/styles/material.css";
import "../../node_modules/@syncfusion/ej2-buttons/styles/material.css";
import "../../node_modules/@syncfusion/ej2-react-calendars/styles/material.css";
import { CalendarComponent } from "@syncfusion/ej2-react-calendars";
import { useHistory } from "react-router";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import LocalizedStrings from "react-localization";
import { presentToastWithOptions } from "../shared/toast/toast";

const BookAnAppointmentStep2: React.FC = () => {
  const clinicCode = useParams<{ id: string }>().id;
  const transno = useParams<{ transno: string }>().transno;

  let history: any = useHistory();
  let data: any = history.location.state;
  const [prevAppintmentDetails, setprevAppintmentDetails] = useState<any>(data);

  const [value, setValue] = useState<Date>(new Date());
  const [showModal, setShowModal] = useState(false);
  const [selectedIndex, setselectedIndex] = useState(0);
  const [details, setdetails] = useState<any>();
  const [doctorDetails, setdoctorDetails] = useState<any>();

  const [timeSlotList, settimeSlotList] = useState<any[]>();
  var dateValue: Date = new Date();
  const [maxDate, setmaxDate] = useState<Date>(new Date());
  const [Hospitallocation, setHospitallocation] = useState<string>("2");
  const [ConsultationType, setConsultationType] = useState<string>("1");

  const onChange = useCallback(
    (value) => {
      console.log(value);
      setValue(value?.value);
    },
    [setValue]
  );

  const onClose = () => {
    setShowModal(false);
    confirmBooking();
  };

  let strings = new LocalizedStrings({
    en: {
      bookAppointmentStep2: "Book Appointment Step 2",
      onlineConsultation: "Online",
      physicalAppointment: "Physical",
      chooseTimeSlot: "Choose Time Slot",
      confirmAppointment: "CONFIRM APPOINTMENT",
      availableOnDays: "Available On Days",
      alHilalWestDohaQatar: "AL HILAL WEST, DOHA, QATAR",
      selectLocation: "Select Location",
      alHilalWes: "Al Hilal",
      northARRayyan: "North, AR-RAYYAN",
    },
    ar: {
      bookAppointmentStep2: "مستشفى",
      onlineConsultation: "مستشفى",
      physicalAppointment: "مستشفى",
      chooseTimeSlot: "مستشفى",
      confirmAppointment: "مستشفى",
      availableOnDays: "مستشفى",
      alHilalWestDohaQatar: "مستشفى",
      selectLocation: "مستشفى",
      alHilalWes: "مستشفى",
      northARRayyan: "مستشفى",
    },
  });

  const ConvertDateIntoString = (date: Date): string => {
    var dd = String(date.getDate()).padStart(2, "0");
    var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = date.getFullYear();

    var today1 = dd + "/" + mm + "/" + yyyy;
    return today1;
  };

  const convertDateToMMDD = (dateString: string): any => {
    if (dateString) {
      let initial = dateString.split(/\//);
      let joinString = [initial[1], initial[0], initial[2]].join("/");
      let ddmmyyyyDate = new Date(joinString);
      console.log(ddmmyyyyDate);
      return ddmmyyyyDate;
    } else return new Date();
  };

  const GetDoctorAvailabilityDetails = () => {
    let headers = getAuthHeader();
    console.log(clinicCode);

    axios
      .get(
        endpoints.hostName +
          apiPath.DoctorAvailabilityDetailsApiPath +
          clinicCode,
        { headers }
      )
      .then(
        (response) => {
          if (response && response.data) {
            console.log(response.data);
            let last = Object.keys(response.data?.["next_month"])[
              Object.keys(response.data?.["next_month"])!.length - 1
            ];
            setmaxDate(convertDateToMMDD(last));
            setdetails(response.data);
          }
        },
        (error) => {
          console.error(
            "[BookAppintment page]: get doctor availability details"
          );
          presentToastWithOptions("Booking not available", "error");
          handleAuthError(error, history);
        }
      );
  };

  useEffect(() => {
    if (transno) {
      getAppointmentDetails();
    } else {
      getDoctorDetails();
    }
    GetDoctorAvailabilityDetails();
  }, []);

  const PrepareTimeslotModel = () => {
    let tempTimeSlotList = null;
    if (details && details["curr_month"]) {
      tempTimeSlotList = details["curr_month"][ConvertDateIntoString(value)]
        ? details["curr_month"][ConvertDateIntoString(value)]
        : details["next_month"][ConvertDateIntoString(value)];
    }
    if (tempTimeSlotList) {
      settimeSlotList(tempTimeSlotList);
      if (
        prevAppintmentDetails &&
        prevAppintmentDetails.APPDATE &&
        prevAppintmentDetails.APPTIME
      ) {
        var from = prevAppintmentDetails.APPDATE.toString().split("/");
        var a = new Date(from[2], from[1] - 1, from[0]);
        var dateYYYYMMDD =
          a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
        let preDate = new Date(
          dateYYYYMMDD + " " + prevAppintmentDetails.APPTIME
        );
        let timeStr =
          preDate.getHours().toString().length > 1
            ? preDate.getHours().toString()
            : "0" + preDate.getHours().toString();
        let mint =
          preDate.getMinutes().toString().length > 1
            ? preDate.getMinutes().toString()
            : "0" + preDate.getMinutes().toString();
        tempTimeSlotList?.forEach(
          (item: { slotTime: string }, index: React.SetStateAction<number>) => {
            if (item.slotTime === timeStr + ":" + mint + ":00") {
              setselectedIndex(index);
            }
          }
        );
      }
      // setselectedIndex()
      setShowModal(true);
    } else {
      console.error("not valid time selection");
    }
  };

  const confirmBooking = () => {
    let timeArray = timeSlotList?.[selectedIndex]?.["slotTime"].split(":");
    let setTime = value.setHours(timeArray[0], timeArray[1], 0);
    let dateOptions = {
      weekday: "short",
      year: "numeric",
      month: "short",
      hour: "2-digit",
      minute: "2-digit",
      day: "2-digit",
      hour12: true,
    } as const;
    let date = new Date(setTime).toLocaleString(undefined, dateOptions);
    localStorage.setItem(
      "apointmentDetail",
      JSON.stringify({
        CONSULTTYPE: ConsultationType,
        selectedDate: setTime,
        dateString: date,
        slotTime: timeSlotList?.[selectedIndex].slotTime,
        CLINICCODE: timeSlotList?.[selectedIndex].CLINICCODE,
        transNo: transno ? transno : "",
        clinicdesc: doctorDetails?.CLINICDESC,
        deptdesc: doctorDetails?.DEPTDESC,
        location: Hospitallocation,
      })
    );
    history.push({ pathname: "/BookAnAppointmentStep3" });
  };
  const getDoctorDetails = () => {
    let headers = getAuthHeader();

    axios
      .get(endpoints.hostName + apiPath.doctorDetailsApiPath + clinicCode, {
        headers,
      })
      .then(
        (response: any) => {
          if (response && response.data) {
            console.log("doctor details", response.data);
            if (response.data) {
              //  prePopulatePrevDate()
              if (response.data.location && response.data.location.id) {
                setHospitallocation(response.data.location.id);
              }
              setdoctorDetails(response.data);
            }
          }
        },
        (error) => {
          console.error("[Book appointment 2 page]: Get doctor details");
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        }
      );
  };
  const getAppointmentDetails = () => {
    let headers = getAuthHeader();

    axios
      .get(endpoints.hostName + apiPath.AppointmentByIdApiPath + transno, {
        headers,
      })
      .then(
        (response: any) => {
          if (response && response.data) {
            console.log("doctor details", response.data);
            if (response.data && response.data[0]) {
              if (response.data[0].location && response.data[0].location.id) {
                setHospitallocation(response.data[0].location.id);
              }
              setprevAppintmentDetails(response.data[0]);
              setdoctorDetails(response.data[0]);
              prePopulatePrevDate(
                response.data[0].APPDATE,
                response.data[0].APPTIME
              );
            }
          }
        },
        (error) => {
          console.error("[Book appointment 2 page]: Get appointment details");
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        }
      );
  };
  const prePopulatePrevDate = (date: any, time: any) => {
    console.log("am 1st");
    console.log("this is tran no", prevAppintmentDetails);
    if (date && time) {
      var from = date.toString().split("/");
      var a = new Date(from[2], from[1] - 1, from[0]);
      var dateYYYYMMDD =
        a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
      let preDate = new Date(dateYYYYMMDD + " " + time);
      setValue(preDate);
      let timeStr =
        preDate.getHours().toString().length > 1
          ? preDate.getHours().toString()
          : "0" + preDate.getHours().toString();
      let mint =
        preDate.getMinutes().toString().length > 1
          ? preDate.getMinutes().toString()
          : "0" + preDate.getMinutes().toString();
      timeSlotList?.forEach((item, index) => {
        if (item.slotTime === timeStr + ":" + mint) {
          setselectedIndex(index);
        }
      });
      console.log(value);
    }
  };
  return (
    <IonPage>
      <Header title={strings.bookAppointmentStep2} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="b2-grid">
          <IonRow class="b2-rowradioStyle">
            <IonCol>
              <fieldset>
                <legend>Consultation</legend>
                <IonRadioGroup
                  value={ConsultationType}
                  onIonChange={(e) => setConsultationType(e.detail.value)}
                >
                  <IonRadio value="2" class="b2-radioStyle" />
                  <IonLabel>
                    <b>{strings.physicalAppointment}</b>
                  </IonLabel>
                  <IonRadio value="1" class="b2-radioStyle" />
                  <IonLabel>
                    <b>{strings.onlineConsultation}</b>
                  </IonLabel>
                </IonRadioGroup>
              </fieldset>
            </IonCol>
            <IonCol>
              <fieldset>
                <legend>Location</legend>
                <IonRadioGroup
                  value={Hospitallocation}
                  onIonChange={(e) => setHospitallocation(e.detail.value)}
                >
                  <IonRadio value="2" class="b2-radioStyle" />
                  <IonLabel>
                    <b>{strings.alHilalWes}</b>
                  </IonLabel>
                  <IonRadio value="1" class="b2-radioStyle" />
                  <IonLabel>
                    <b> {strings.northARRayyan}</b>
                  </IonLabel>
                </IonRadioGroup>
              </fieldset>
            </IonCol>
          </IonRow>
          <IonRow class="b2-row">
            <IonCol size="2">
              <IonImg src={Doctor}></IonImg>
            </IonCol>
            <IonCol size="10" class="b2-textCol">
              <IonText class="b2-text1">
                <b className="fontHeading">
                  {"DR." + " " + doctorDetails?.CLINICDESC}
                </b>
              </IonText>
              <IonText class="b2-text2">{doctorDetails?.DEPTDESC}</IonText>
              {/* <IonText class="b2-text3">INTERNAL MEDICINE CONSULTANT</IonText> */}
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <div>
                {/* https://ej2.syncfusion.com/react/documentation/calendar/getting-started/ */}

                <CalendarComponent
                  id="calendar"
                  value={value}
                  min={dateValue}
                  max={maxDate}
                  // renderDayCell={disableDays}
                  onChange={onChange}
                />
              </div>
              <p>
                <IonText className="fontHeading"> Date Selected:  </IonText>
                {value.toDateString()}
              </p>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonModal isOpen={showModal} cssClass="my-custom-class">
                <IonHeader translucent>
                  <IonToolbar>
                    <IonTitle>CHOOSE TIME SLOT</IonTitle>
                    <IonButtons slot="end">
                      <IonButton onClick={onClose}>Confirm</IonButton>
                    </IonButtons>
                  </IonToolbar>
                </IonHeader>
                <IonContent fullscreen>
                  <IonGrid>
                    <IonRow>
                      {timeSlotList?.map((item, index) => {
                        if (item.availableStatus) {
                          return (
                            <IonCol size="6" key={index}>
                              <IonButton
                                expand="full"
                                onClick={() => setselectedIndex(index)}
                                color={
                                  selectedIndex === index ? "light" : "primary"
                                }
                                disabled={!item.availableStatus}
                              >
                                {item.slotTime}
                              </IonButton>
                            </IonCol>
                          );
                        }
                      })}
                    </IonRow>
                  </IonGrid>
                </IonContent>
              </IonModal>
              <IonButton onClick={PrepareTimeslotModel} size="small">
                {strings.chooseTimeSlot}
              </IonButton>{" "}
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep2;

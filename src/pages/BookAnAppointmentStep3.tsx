import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonText,
  useIonViewWillEnter,
  useIonViewWillLeave,
} from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import Doctor from "../images/Doctor-01.png";
import "./BookAnAppointmentStep3.css";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil"
import LocalizedStrings from "react-localization";
import { useHistory } from "react-router";
import {toastPromises,presentToastWithOptions} from "../shared/toast/toast";


const BookAnAppointmentStep3: React.FC = () => {
  interface Idetails {
    name: string;
    department: string;
    type: string;
    lastVist: string;
  }
  let history: any = useHistory();

  const [apointmentDetail, setapointmentDetail] = React.useState(() => {
    const stickyValue = localStorage.getItem("apointmentDetail");
    return stickyValue !== null ? JSON.parse(stickyValue) : "";
  });
  const [userDetails, setuserDetails] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue) : "";
  });
  useIonViewWillEnter(() => {
    if (!apointmentDetail || apointmentDetail === "") {
      history.goBack();
    }
  });

  useIonViewWillLeave(() => {
    console.log("ionViewWillLeave event fired");
    localStorage.removeItem("apointmentDetail");
  });
  const arrayOfDetails: Array<Idetails> = [
    {
      name: "DR. AHMED GAAFAR ZABADY",
      department: "INTERNAL MEDICINE",
      lastVist: "26 August 2021, 1:30 PM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
  ];

  let strings = new LocalizedStrings({
    en: {
      bookAppointmentStep3: "Book Appointment Step 3",
      appointmentDateAndTime: "Appointment Date & Time",
      edit: "Edit",
      cancel: "Cancel",
      done: "Confirm",
      successful:"Appointment booked successfully.",
      alreadyBooked:'Slot is not available.',
      updateSucessful:"Appointment updated successfully.",
      updateFail:"Failed to update appointment."
    },
    ar: {
      bookAppointmentStep3: "مستشفى",
      appointmentDateAndTime: "مستشفى",
      edit: "مستشفى",
      cancel: "مستشفى",
      done: "مستشفى",
    },
  });
  const bookAppointment = () => {
    let date = new Date(apointmentDetail.selectedDate);
    let utcDateFormat = date.toISOString();
    console.log(date, utcDateFormat);
    let reqdata = {
      APPOINTMENTID: apointmentDetail?.transNo,
      FNAME: userDetails?.FNAME,
      LNAME: userDetails?.LNAME,
      MOBILE: userDetails?.MOBILE,
      CLINICCODE: apointmentDetail?.CLINICCODE,
      DATE: utcDateFormat,
      CONSULTTYPE: 1,
      PATCODE: userDetails?.PATCODE,
      LOCATION:apointmentDetail?.location,
    };
    let headers = getAuthHeader();
    
    axios
      .post(endpoints.hostName + apiPath.bookAppointmentApiPath, reqdata, {headers})
      .then(
        (response: any) => {
          toastPromises(strings.successful,'success').then(()=>{
            history.push({ pathname: "/main" });
          },(error)=>{
            console.log('reject')
          })
          console.log("[Book Appointment 3 page]:  successful");
        },
        (error) => {
          console.log("[Book Appointment 3 page]:  Error");
          presentToastWithOptions(strings.alreadyBooked,'error');
          handleAuthError(error,history);


        }
      );
  };
  const updateAppointment = () => {
    let date = new Date(apointmentDetail.selectedDate);
    let utcDateFormat = date.toISOString();
    let reqData={
      "APPOINTMENTID": apointmentDetail.transNo,
      "DATE": utcDateFormat
    }
    let headers = getAuthHeader();
   
    axios
    .post<any>(endpoints.hostName + apiPath.updateAppointmentApiPath,reqData, { headers}) //"http://34.131.82.122:3001/alemadi/mobile/patient/appointment/patcode/270198?completed=true")
    .then(
      (response: any) => {
        toastPromises(strings.updateSucessful,'success').then(()=>{
          history.push({ pathname: "/main" });
        },(error)=>{
          console.log('reject')
        })
        console.log("[Book Appointment 3 page]:  successful");
      },
      (error) => {
        console.log("[Book Appointment 3 page]:  Error");
        presentToastWithOptions(strings.updateFail,'error');
        handleAuthError(error,history);


      }
    )};
  const goBack = () => {
    history.goBack();
  };

  return (
    <IonPage>
      <Header title={strings.bookAppointmentStep3} defaultRoute={"/Main"} />
      <IonContent>
        {arrayOfDetails.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.name} class="BcardStyle">
            <IonCardContent class="BcardContentStyle">
              <IonGrid>
                <IonRow class="BfirstRow">
                  <IonCol size="2">
                    <IonImg src={Doctor}></IonImg>
                  </IonCol>
                  <IonCol size="10" class="BtextCol">
                    <IonText class="Btext1">
                    <b className="fontHeading">{'DR.'+' '+apointmentDetail?.clinicdesc}</b>
                    </IonText>
                    <IonText class="Btext2">{apointmentDetail?.deptdesc}</IonText>
                    {/* <IonText class="Btext3">{item.type}</IonText> */}
                  </IonCol>
                </IonRow>
                <IonRow class="BsecondRow">
                  <IonCol>
                    <IonText>
                      <b>{strings.appointmentDateAndTime}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="BthirdRow">
                  <IonCol>
                    <IonText>
                      <b>{apointmentDetail.dateString}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol class="editCol">
                    <IonButton
                      size="small"
                      expand="full"
                      shape="round"
                      onClick={goBack}
                      // routerLink="/BookAnAppointmentStep2"
                    >
                      <b>{strings.edit}</b>
                    </IonButton>
                  </IonCol>
                  <IonCol class="cancelCol">
                    <IonButton
                      size="small"
                      expand="full"
                      shape="round"
                      routerLink="/Main"
                    >
                      <b>{strings.cancel}</b>
                    </IonButton>
                  </IonCol>
                  <IonCol class="cancelCol">
                    <IonButton
                      size="small"
                      expand="full"
                      shape="round"
                      onClick= {!apointmentDetail?.transNo  ? bookAppointment : updateAppointment}
                      // onClick={bookAppointment}
                      // routerLink="/YourDoctors"
                      // routerLink="/Main"
                    >
                      <b>{strings.done}</b>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep3;

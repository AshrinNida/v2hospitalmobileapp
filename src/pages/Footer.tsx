import {
  IonFooter,
  IonRoute,
  IonRouterLink,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import "./Footer.css";

const Footer: React.FC = () => {
  return (
    <IonFooter>
      <IonToolbar className="ionFooterToolbar">
        <IonRouterLink color="light" href="/Hospitalinformation">
          <IonTitle
            style={{
              color: "white",
              backgroundColor: "rgb(82,97,156)",
              textAlign: "center",
            }}
          >
            www.alemadihospital.com.qa
          </IonTitle>
        </IonRouterLink>
      </IonToolbar>
    </IonFooter>
  );
};

export default Footer;

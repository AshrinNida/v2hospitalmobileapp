import React, { useEffect, useRef, useState } from "react";
import {
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonButton
} from "@ionic/react";
import "./Main.css";
import { getAuthHeader } from "../shared/commonUtil";
import axios from "axios";
import HomePageIcon1 from "../images/HomePageIcones-01.png";
import HomePageIcon2 from "../images/HomePageIcones-02.png";
import HomePageIcon3 from "../images/HomePageIcones-03.png";
import HomePageIcon4 from "../images/HomePageIcones-04.png";
import HomePageIcon5 from "../images/HomePageIcones-05.png";
import HomePageIcon6 from "../images/HomePageIcones-06.png";
import HomePageIcon7 from "../images/HomePageIcones-07.png";
import Footer from "./Footer";
import Header from "./Header";
import { Network } from "@capacitor/network";
import { endpoints, apiPath } from "../shared/application_constants";

import LocalizedStrings from "react-localization";
import { presentToastWithOptions } from "../shared/toast/toast";

const Main: React.FC = () => {
  const [showToast, setShowToast] = useState<boolean>(false);
  const [Disabled, setDisabled] = useState<boolean>(false);

  const [PATCODE, setPATCODE] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).PATCODE : "N/A";
  });
  const [FNAME, setFNAME] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).FNAME : "N/A";
  });
  const [QID, setQID] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).QID : "N/A";
  });
  interface Idetails {
    icon: any;
    url: string;
    disable: boolean;
  }
  const arrayOfMainOPtions: Array<Idetails> = [
    {
      icon: HomePageIcon1,
      url: "/FindDoctor",
      disable: false,
    },
    {
      icon: HomePageIcon2,
      url: "/BookAnAppointmentStep1/external",
      disable: false,

    },
    {
      icon: HomePageIcon3,
      url: "/Prescription",
      disable: false,

    },
    {
      icon: HomePageIcon4,
      url: "/YourDoctors",
      disable: false,

    },
    {
      icon: HomePageIcon5,
      url: "/LabReport",
      disable: false,

    },
    {
       icon: HomePageIcon6,
       url: "/Pharmacy",
       disable: true,

     },
    {
      icon: HomePageIcon7,
      url: "/InsuranceApproval",
      disable: false,

    },
  ];


const [image, setImage] = useState("");
 let headers = getAuthHeader();

  Network.addListener('networkStatusChange', status => {
    console.log('Network status changed', status);
       if (status.connected){
        setShowToast(true);
        presentToastWithOptions('Network Connected');
      }else{
        setShowToast(false);
        presentToastWithOptions('Network was disconnected','error');
      }
      });
   

  let strings = new LocalizedStrings({
    en: {
      welcomeToAlEmadiHospital: "Welcome To Al Emadi Hospital",
      name: "Name",
      fileID: "File ID",
      qatarID: "Qatar ID",
    },
    ar: {
      welcomeToAlEmadiHospital: "مستشفى",
      name: "مستشفى",
      fileID: "مستشفى",
      qatarID: "مستشفى",
    },
  });

  

  return (
    <IonPage>
      <Header title={strings.welcomeToAlEmadiHospital} defaultRoute={""}/>
      <IonContent>
        <IonGrid class="grid">
          <IonRow>
            <IonCol class="details" size="3">
              <b>{strings.name}</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b>:</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>{FNAME}</b>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="details" size="3">
              <b>{strings.fileID}</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b> :</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>{PATCODE}</b>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="details" size="3">
              <b>{strings.qatarID}</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b>:</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>{QID}</b>
            </IonCol>
          </IonRow>
          <IonRow class="divider">
            <IonCol className="colBarcode">
              <div className="imgContainer"><img className="barcode" id="barcodeImg" src={endpoints.hostName + apiPath.apiPathBarcode  + QID} /></div>
            </IonCol>
          </IonRow>
          <IonRow>
            {arrayOfMainOPtions.map((items) => (
              /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
              <IonCol key={items.icon} size="4" class="colContent">
                <div>
                 <a href={items.url}>
                   <img src={items.icon} alt="icon missing"></img> </a>
                </div>
              </IonCol>
            ))}
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Main;



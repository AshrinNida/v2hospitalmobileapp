import React, { useEffect, useState } from "react";
import {
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
  IonLoading,
  IonList,
  IonItem,
  IonButton,
} from "@ionic/react";
import Footer from "../Footer";
import "react-accessible-accordion/dist/fancy-example.css";
import "./LabReport.css";
import axios from "axios";
import { endpoints, apiPath } from "../../shared/application_constants";
import Header from "../Header";
import { useHistory } from "react-router";
import { presentToastWithOptions } from "../../shared/toast/toast";
import { Browser } from "@capacitor/browser";
import { getAuthHeader, handleAuthError } from "../../shared/commonUtil";

const LabReport: React.FC = () => {
  // let labReports:any=null
  const [labReports, setLabReports] = useState<any[]>([]);
  const [showLoadingAnimation, setShowLoadingAnimation] = useState(true);
  const [PATCODE, setPATCODE] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).PATCODE : "N/A";
  });
  let history: any = useHistory();

  useEffect(() => {
    const getPatientReportUsingPatId = () => {
      let headers = getAuthHeader();

      axios
        .get(endpoints.hostName + apiPath.getPatientReportApuPath + PATCODE, {
          headers,
        })
        .then((data: any) => {
          if (data && data.status == 200) {
            console.log(
              "[Lab reports: getPatientReportsUsingPatId function] data from server: ",
              data
            );
            if (
              data.data &&
              data.data.reports &&
              data.data.reports.length > 0
            ) {
              setLabReports(data.data.reports);
              ViewReportPdf(data.data.reports)
            }
          } else {
            // Todo handle error here
          }

          //sorting of dates
          let Newdata = data.data.reports;
          function dateComparison(a: { date: Date }, b: { date: Date }) {
            const date1 = new Date(a.date).getTime();
            const date2 = new Date(b.date).getTime();

            return date2 - date1;
          }
          var sortedData = Newdata.sort(dateComparison);

          console.log(sortedData);
        })
        .catch((error) => {
          // Todo handle error here
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        })
        .finally(() => {
          // close the loading animation here
          setShowLoadingAnimation(false);
        });
    };
    getPatientReportUsingPatId();
  }, []);

  const ViewReportPdf = (date: any) => {
    var pdfEndPoint = PreparePfUrl(date);
    if (Browser) {
      Browser.open({ url: pdfEndPoint });
    } else {
      history.push({
        pathname: "/PdfViewerParent",
        state: {
          pdfURL: pdfEndPoint,
          btnName: "Close",
        },
      });
    }
  };

  const PreparePfUrl = (d: string) => {
    console.log(d);
    var pdfEndPoint =
      endpoints.hostName +
      apiPath.ReportPdfApiPath +
      "&token=" +
      localStorage.getItem("token");
    pdfEndPoint = pdfEndPoint.replace("{0}", PATCODE);
    pdfEndPoint = pdfEndPoint.replace("{1}", d);
    console.log(pdfEndPoint);
    return pdfEndPoint;
  };

  return (
    <IonPage>
      <Header title="Lab Reports" defaultRoute={"/main"} />
      <IonContent>
        <IonGrid class="labGrid">
          <IonRow>
            <IonCol class="labGridCol">
              {labReports && (
                <IonList>
                  {labReports.map((report: any, i) => (
                    <IonItem key={i} class="testitem">
                      <b>
                        {report.test[0].name + " "}
                        {report.date}
                      </b>
                      <IonButton
                        slot="end"
                        onClick={() => ViewReportPdf(report.date)}
                      >
                        View Report
                      </IonButton>
                    </IonItem>
                  ))}
                </IonList>
              )}
              <IonLoading
                isOpen={showLoadingAnimation}
                message={"Loading..."}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default LabReport;

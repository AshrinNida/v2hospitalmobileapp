import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonHeader,
  IonIcon,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { logOutOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router";
import { presentToastWithOptions } from "../shared/toast/toast";
import "./Header.css";

const Header: React.FC<{ title: string; defaultRoute: string }> = (props) => {
 const history = useHistory();
  const logout = () =>{
    localStorage.clear();
    history.push('/login');
    presentToastWithOptions('Logged Out successfully');

  }
  
  return (
    <IonHeader>
      <IonToolbar class="ionToolbar">
        <IonButtons slot="start">
          {props.defaultRoute && (
            <IonBackButton defaultHref={props.defaultRoute} />
          )}
        </IonButtons>
        <IonButtons slot="secondary" >
      <IonButton onClick={logout}>
        <IonIcon slot="start" icon={logOutOutline} />
      </IonButton>
    </IonButtons>
        <IonTitle>{props.title}</IonTitle>
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;

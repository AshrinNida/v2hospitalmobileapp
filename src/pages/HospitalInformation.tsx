import {
  IonPage,
  IonTitle,
  IonCard,
  IonIcon,
  IonRouterLink,
  IonButton,
  IonBackButton,
  IonRow,
  IonCol,
} from "@ionic/react";
import HospitalLogo from "../images/hospital-logo.png";
import "./HospitalInformation.css";
import {
  call,
  location,
  logoFacebook,
  logoInstagram,
  mail,
  time,
} from "ionicons/icons";
import Header from "./Header";

const HospitalInformation: React.FC = () => {
  return (
    <IonPage className="background">
      <Header title={"LOCATION"} defaultRoute={"/main"} />
      <IonRow>
        <IonCol>
          <img src={HospitalLogo} className="logo"></img>
        </IonCol>
      </IonRow>
      <IonCard style={{overflow:"scroll"}}>
        <p>
          <IonIcon icon={location} style={{ width: "50px" }} />
          Al Emadi Hospital - Al Hilal West, Doha,{" "}
        </p>
        <p style={{ marginLeft: "49px" }}> Qatar PO.Box : 50000</p>

        <p>
          <IonIcon icon={call} style={{ width: "50px" }} />
          +974 4477 6444{" "}
        </p>

        <p>
          <IonIcon icon={time} style={{ width: "50px" }} />
          Saturday - Thursday: 8AM to 10PM,{" "}
        </p>
        <p style={{ marginLeft: "49px" }}>Emergency-Pharmacy-Laboratory-24/7</p>

        <IonRouterLink href="https://www.google.com/maps/place/Al+Emadi+Hospital/@25.255756,51.534214,13z/data=!4m5!3m4!1s0x0:0x14ed6fea7d5b4cfb!8m2!3d25.2557563!4d51.5342141?hl=en">
          <IonButton style={{ marginLeft: "50px" }} color="dark">
            Direction
          </IonButton>
        </IonRouterLink>

        <br />
        <p>
          <IonIcon icon={location} style={{ width: "50px" }} />
          Al Emadi Hospital Clinics - North,{" "}
        </p>
        <p style={{ marginLeft: "49px" }}> Al Rayyan, Qatar.</p>
        <p>
          <IonIcon icon={call} style={{ width: "50px" }} />
          +974 4477 6444{" "}
        </p>

        <p>
          <IonIcon icon={time} style={{ width: "50px" }} />
          Saturday - Wednesday: 9AM to 9PM,{" "}
        </p>
        <p style={{ marginLeft: "49px" }}>Thursday: 9AM to 6PM, Friday: OFF</p>
        <IonRouterLink href="https://www.google.com/maps?ll=25.358419,51.449022&z=14&t=m&hl=en&gl=IN&mapclient=embed&cid=5175871007797234915">
          <IonButton style={{ marginLeft: "50px" }} color="dark">
            Direction
          </IonButton>
        </IonRouterLink>

        <br />

        <p>
          <IonIcon icon={mail} style={{ width: "50px" }} />
          info@alemadihospital.com.qa{" "}
        </p>
        <br />
        <div style={{ marginLeft: "150px" }}>
          <IonRouterLink
            href="https://www.facebook.com/Emadi.Hospital/"
            className="icon"
          >
            <IonIcon icon={logoFacebook} size="large"></IonIcon>
          </IonRouterLink>
          &nbsp;&nbsp;
          <IonRouterLink
            href="https://www.instagram.com/alemadi.hospital/"
            className="icon"
          >
            <IonIcon icon={logoInstagram} size="large"></IonIcon>
          </IonRouterLink>
          &nbsp;&nbsp;
        </div>
      </IonCard>
    </IonPage>
  );
};

export default HospitalInformation;

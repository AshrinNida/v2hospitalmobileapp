import React, { useEffect, useState } from "react";
import {
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
  IonLoading,
  IonCardContent,
  IonCard,
} from "@ionic/react";
import Footer from "../Footer";
import "react-accessible-accordion/dist/fancy-example.css";
import "./orders.css";
import axios from "axios";
import { endpoints, apiPath } from "../../shared/application_constants";
import Header from "../Header";
import { useHistory } from "react-router";
import { getAuthHeader, handleAuthError } from "../../shared/commonUtil";
import { presentToastWithOptions } from "../../shared/toast/toast";
const Orders: React.FC = () => {
  const [ordersList, setordersList] = useState<any[]>([]);
  const [ordersDate, setordersDate] = useState<any>('');

  const [showLoadingAnimation, setShowLoadingAnimation] = useState(true);
  const [PATCODE, setPATCODE] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).PATCODE : "N/A";
  });
  let history: any = useHistory();

  useEffect(() => {
    const getOrdersPatId = () => {
      let headers = getAuthHeader();
      axios
        .get(endpoints.hostName + apiPath.ordersApiPath + PATCODE, {headers})
        .then((data: any) => {
          if (data && data.status == 200) {
            console.log(
              "[Orders: getOrdersPatId function] data from server: ",
              data
            );
            if (
              data.data &&
              data.data) {
              setordersList(data.data);
              setordersDate(data.data[0]?.VISTDATE)
            }
          } else {
            // Todo handle error here
          }
        })
        .catch((error) => {
          presentToastWithOptions('something went wrong.','error');
          handleAuthError(error,history);
        })
        .finally(() => {
          // close the loading animation here
          setShowLoadingAnimation(false);
        });
    };
    getOrdersPatId();
  }, []);


  return (
    <IonPage>
      <Header title={'Orders'+' '+ ordersDate} defaultRoute={"/main"} />
      <IonContent className="font">
        <IonGrid class="order-grid">
          <IonRow>
            <IonCol class="order-gridCol">
              {ordersList.map((order, i) => ( 
                <IonCard className="fontColor" key={i}>
                <IonCardContent style={{color:"#91918c"}}>
                <IonRow>
                    <IonCol size="12"><b className="fontHeading">BILLDESC:</b> {order.BILLDESC}</IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol size="12"><b className="fontHeading">BILLCODE:</b> {order.BILLCODE}</IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol size="12"><b className="fontHeading">SERVTYPE:</b> {order.SERVTYPE}</IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol size="12"><b className="fontHeading">VISITID:</b> {order.VISITID}</IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
              ))}

              <IonLoading
                isOpen={showLoadingAnimation}
                message={"Loading..."}
              />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Orders;

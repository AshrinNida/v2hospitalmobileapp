import React, { useEffect, useState } from "react";
import {
  IonContent,
  IonPage,
  IonSearchbar,
  IonText,
  IonRow,
  IonCard,
  IonCardContent,
  IonCol,
  IonGrid,
} from "@ionic/react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from "react-accessible-accordion";
import Footer from "./Footer";
import Header from "./Header";
import "react-accessible-accordion/dist/fancy-example.css";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import { presentToastWithOptions } from "../shared/toast/toast";
import { useHistory } from "react-router";
import { nextUuid } from "react-accessible-accordion/dist/types/helpers/uuid";

export const Pharmacy: React.FC = () => {
  const [searchText, setSearchText] = useState("");
  const [details, setdetails] = useState<Idetails[]>([]);
  let history: any = useHistory();
  let arrayOfDetails: Idetails[] = [];

  interface Idetails {
    locationDesc: string;
    locationCode: string;
    inStock: string;
    itemCode: any;
    itemDesc: string;
    qtyHand: string;
  }

  const GetPharmacyList = () => {
    let headers = getAuthHeader();

    axios
      .get<any>(endpoints.hostName + apiPath.PharmacyApiPath+ searchText, { headers }) //"http://34.131.82.122:3001/alemadi/mobile/pharmacy/search?itemcode=PHC00001")
      .then(
        (response) => {
          console.log(response.data);
          PopulatePharmacy(response.data);
        },
        (error) => {
          console.log("[Pharmacy]: Error Fetching");
          presentToastWithOptions("Something went wrong", "error");
          handleAuthError(error, history);
          alert(error);
        }
      );
  };

  useEffect(() => {
    GetPharmacyList();
  }, [searchText]);

  const PopulatePharmacy = (data: any) => {
    arrayOfDetails = [];
    data.forEach((d: any) => {
      arrayOfDetails.push({
        locationDesc: d.LOCDESC,
        itemCode: d.ITEMCODE,
        locationCode: d.LOCCODE,
        inStock: d.INSTOCK,
        itemDesc: d.ITEMDESC,
        qtyHand: d.QTYHAND,
      });
    });
    setdetails(arrayOfDetails);
  };

  var stock = function (inStock: string) {
    if (inStock) {
      return "YES";
    } else {
      return "NO";
    }
  };

  return (
    <IonPage>
      <Header defaultRoute={"/main"} title={"Pharmacy"} />
      <IonContent>
        <IonSearchbar
          value={searchText}
          onIonChange={(e) => setSearchText(e.detail.value!)}
          animated
        ></IonSearchbar>

        {details
          .map((e, i) => (
            <Accordion key={i} preExpanded={['pharm']}>
              <AccordionItem uuid="pharm">
                <AccordionItemHeading id="Toggle" className="fontColor">
                  <AccordionItemButton 
                    style={{
                      backgroundColor: "#5a62ad",
                      color: "white",
                      width: "90% ",
                      marginLeft: "5%",
                    }}
                  >
                    {e.locationDesc}
                  </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel >
                  <IonGrid>
                    <IonCard>
                      <IonCardContent>
                        <IonRow>
                          <IonCol>
                            <IonText>
                              <b className="fontHeading">
                                ITEM CODE:
                              </b>{" "}
                              {e.itemCode}
                            </IonText>
                            <br></br>
                            <IonText>
                              <b className="fontHeading">ITEM:</b>{" "}
                              {e.itemDesc}
                            </IonText>
                            <br></br>
                            <IonText>
                              <b className="fontHeading">IN STOCK: </b>
                              {stock(e.inStock)}
                            </IonText>
                            <br></br>
                          </IonCol>
                        </IonRow>
                      </IonCardContent>
                    </IonCard>
                  </IonGrid>
                </AccordionItemPanel>
              </AccordionItem>
            </Accordion>
          ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Pharmacy;

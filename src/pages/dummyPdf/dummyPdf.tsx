import React, {  } from "react";
import './dummyPdf.css'
import { IonContent, IonPage } from "@ionic/react";
import { useHistory } from "react-router";



const DummyPdf: React.FC = () => {
    let history: any = useHistory();

    return (
        <IonPage>
            <IonContent style={{background:"white"}}>
                <div style={{height:"100%",background:"white"}}>

               <div className="pdf_holder" style={{color:"black"}} onClick={()=>{
                    console.log("gg");
                    history.push({ pathname: "/PdfViewerParent", state: {pdfURL:"http://localhost:8100/assets/pdf/sample.pdf",btnName:"Acknowledged"} });
               } }>
                   open PDF
               </div>

                </div>
            </IonContent>
        </IonPage>
    )
}

export default DummyPdf;


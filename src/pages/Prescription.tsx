import {
  IonBadge,
  IonButton,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import Doctor from "../images/Doctor-01.png";
import "./Prescription.css";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import LocalizedStrings from "react-localization";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import { useHistory } from "react-router";
import { Browser } from '@capacitor/browser';
import { presentToastWithOptions } from "../shared/toast/toast";

const Prescription: React.FC = () => {
  let history: any = useHistory();
  var temp_list: string[] = [];
  let arrayOfDetails: Idetails[] = [];
  const [list, setlist] = useState<string[]>([]);
  const [details, setdetails] = useState<Idetails[]>([]);
  const [PATCODE, setPATCODE] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).PATCODE : "N/A";
  });

  interface Idetails {
    name: string;
    department: string;
    lastVist: string;
    date: string;
    iconURL: string;
  }

  const GeAppointmentList = () => {
    let headers = getAuthHeader();
  
    axios
      .get<any>(endpoints.hostName + apiPath.CompletedAppointmentApiPath, { headers }) //"http://34.131.82.122:3001/alemadi/mobile/patient/appointment/patcode/270198")
      .then((response) => {
        console.log(response.data);
        PopulatePrescription(response.data);
      },
      (error) => {
        console.log("[Prescription]: Error Fetching");
        presentToastWithOptions('Something went wrong','error');
        handleAuthError(error,history);

      });
  };

  useEffect(() => {
    GeAppointmentList();
  }, []);

  const PopulatePrescription = (data: any) => {
    arrayOfDetails = [];
    temp_list = [];
    data.forEach((d: any) => {
      let appointTime: string;
      var from = d.APPDATE.toString().split("/");
      var a = new Date(from[2], from[1] - 1, from[0]);
      appointTime =
        a.getDate() +
        " " +
        a.toLocaleString("default", { month: "short" }) +
        " " +
        a.getFullYear() +
        ", " +
        d.APPTIME;
      var dateYYYYMMDD =
        a.getFullYear() + "-" + a.getMonth() + "-" + a.getDate();
      console.log(appointTime);
      console.log(dateYYYYMMDD);
      arrayOfDetails.push({
        name: d.CLINICDESC,
        department: d.DEPTDESC,
        lastVist: appointTime,
        date: dateYYYYMMDD,
        iconURL: d.ICONURL,
      });
    });
    setlist(temp_list);
    setdetails(arrayOfDetails);
  };

  let strings = new LocalizedStrings({
    en: {
      prescription: "Prescription",
      viewPrescription: "View Prescription",
      doctorBy: "Doctor By",
    },
    ar: {
      prescription: "مستشفى",
      viewPrescription: "مستشفى",
      doctorBy: "مستشفى",
    },
  });

  const ViewPdf = (date: any) => {
    var pdfEndPoint = PreparePfUrl(date);

    if(Browser) {
      Browser.open({ url: pdfEndPoint});
    }else {
      history.push({
      pathname: "/PdfViewerParent",
      state: {
        pdfURL: pdfEndPoint,
        btnName: "Close",
      },
    });
    }
  };

  const PreparePfUrl = (d: string) => {
    console.log(d);
    var pdfEndPoint = endpoints.hostName + apiPath.PrescriptionApiPath + "&token=" + localStorage.getItem('token');
    pdfEndPoint = pdfEndPoint.replace("{0}", PATCODE);
    pdfEndPoint = pdfEndPoint.replace("{1}", d);
    console.log(pdfEndPoint);
    return pdfEndPoint;
  };

  return (
    <IonPage>
      <Header title={strings.prescription} defaultRoute={"/Main"} />
      <IonContent>
        {details.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.lastVist} class="p-cardStyle">
            <IonCardContent class="p-cardContentStyle">
              <IonGrid>
                <IonRow class="p-firstRow">
                  <IonCol class="p-firstRowCol">
                    <IonText>
                      <b>DATE: {item.lastVist}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="p-secondRow">
                  <IonCol class="p-secondRowCol">
                    <IonText>
                      <b>{strings.doctorBy}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="p-firstRow">
                  <IonCol size="2">
                    <IonImg src={endpoints.hostName + item.iconURL}></IonImg>
                  </IonCol>
                  <IonCol size="10" class="p-textCol">
                    <IonText class="p-text1">
                      <b className="fontHeading">{item.name}</b>
                    </IonText>
                    <IonText class="p-text2">{item.department}</IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButton class="p-btn" onClick={() => ViewPdf(item.date)}>
                      <b>{strings.viewPrescription}</b>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Prescription;

import { IonContent, IonPage } from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";

import LocalizedStrings from 'react-localization';

const ForgotPassword: React.FC = () => {

  let strings = new LocalizedStrings({
    en:{
      forgotpasswordpage:"Forgot password page"
     
    },
    ar: {
      forgotpasswordpage:"مستشفى",
     
    }
    }
   );

  return (
    <IonPage>
      <Header title={strings.forgotpasswordpage} defaultRoute={"/"} />
      <IonContent></IonContent>
      <Footer />
    </IonPage>
  );
};

export default ForgotPassword;

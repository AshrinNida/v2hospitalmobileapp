import {
  IonBadge,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonImg,
  IonLoading,
  IonModal,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
  useIonAlert,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./YourDoctors.css";
import Doctor from "../images/Doctor-01.png";
import { useHistory } from "react-router";
import LocalizedStrings from "react-localization";
import axios from "axios";
import { apiPath, endpoints } from "../shared/application_constants";
import { presentToastWithOptions } from "../shared/toast/toast";
import { Browser } from "@capacitor/browser";
import { getAuthHeader, handleAuthError } from "../shared/commonUtil";
import { image } from "ionicons/icons";

const YourDoctors: React.FC = () => {
  // const tt= Toast('ff')
  const [diagnosisDetails, setdiagnosisDetails] = useState<any[]>([]);
  const [showLoadingAnimation, setShowLoadingAnimation] = useState(true);
  let history: any = useHistory();
  const [present] = useIonAlert();
  var temp_list: string[] = [];
  let arrayOfDetails: Idetails[] = [];
  const [list, setlist] = useState<string[]>([]);
  const [details, setdetails] = useState<Idetails[]>([]);
  const [PATCODE, setPATCODE] = React.useState(() => {
    const stickyValue = localStorage.getItem("userDetails");
    return stickyValue !== null ? JSON.parse(stickyValue).PATCODE : "N/A";
  });
  const [showModal, setShowModal] = useState(false);

  interface Idetails {
    name: string;
    department: string;
    status: string;
    lastVist: string;
    date: string;
    clinicCode: string;
    transNo: string;
    time: string;
    currentStatus: string;
    appStatus: string;
    iconURL: string;
  }

  const GetDoctorList = () => {
    let headers = getAuthHeader();
    axios
      .get<any>(endpoints.hostName + apiPath.AppointmentApiPath + PATCODE, {
        headers,
      }) //"http://34.131.82.122:30013001/alemadi/mobile/patient/appointment/patcode/270198")
      .then(
        (response) => {
          console.log(response.data);
          PopulateDoctor(response.data);
        },
        (error) => {
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        }
      );
  };

  useEffect(() => {
    // Toast.testfun();
    GetDoctorList();
  }, []);

  const PopulateDoctor = (data: any) => {
    arrayOfDetails = [];
    temp_list = [];
    data.forEach((d: any) => {
      let appointTime: string;

      // console.log(d.APPDATE);
      var from = d.APPDATE.toString().split("/");
      var a = new Date(from[2], from[1] - 1, from[0]);
      // console.log(a);
      appointTime =
        a.getDate() +
        " " +
        a.toLocaleString("default", { month: "short" }) +
        " " +
        a.getFullYear() +
        ", " +
        d.APPTIME;
      var dateYYYYMMDD =
        a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
      // console.log(appointTime, dateYYYYMMDD);
      arrayOfDetails.push({
        name: d.CLINICDESC,
        department: "INTERNAL MEDICINE",
        status:
          d.APPSTATUS === 1
            ? "Confirmed"
            : d.APPSTATUS === 2
            ? "Cancelled"
            : "Resheduled",
        lastVist: appointTime,
        date: dateYYYYMMDD,
        clinicCode: d.CLINICCODE,
        transNo: d.TRANSNO,
        time: d.APPTIME,
        currentStatus: d.STATUS,
        appStatus: d.APPSTATUS,
        iconURL: d.ICONURL,
      });
    });
    setlist(temp_list);
    setdetails(arrayOfDetails);
  };

  let strings = new LocalizedStrings({
    en: {
      yourDoctorsPage: "Your Doctors Page",
      lastVisite: "Last Visit",
      diagnosis: "Diagnosis",
      orders: "Orders",
      prescription: "Prescription",
      reschedule: "Reschedule",
      cancel: "Cancel",
      status: [
        "Pending Confirmation",
        "Confirmed",
        "Cancelled",
        "Re-Schedule",
        "Checked In",
        "Checked Out",
        "Waiting List",
        "No Show",
        "Send to Provider",
      ],
    },
    ar: {
      yourDoctorsPage: "مستشفى",
      lastVisite: "مستشفى",
      diagnosis: "مستشفى",
      orders: "مستشفى",
      prescription: "مستشفى",
    },
  });

  const ViewPdfDiagnosis = () => {
    let headers = getAuthHeader();

    axios
      .get(endpoints.hostName + apiPath.diagnosisByAppIdApiPath, { headers })
      .then((data: any) => {
        if (data && data.status == 200) {
          console.log(
            "[your doctor: your doctor ViewPdfDiagnosis] data from server: ",
            data
          );
          if (data.data && data.data) {
            setdiagnosisDetails(data.data);
            setShowModal(true);
          }
        } else {
          // Todo handle error here
          presentToastWithOptions("something went wrong.", "error");
        }
      })
      .catch((error) => {
        // Todo handle error here
        presentToastWithOptions("something went wrong.", "error");
        handleAuthError(error, history);
      })
      .finally(() => {
        // close the loading animation here
        setShowLoadingAnimation(false);
      });
  };

  const ViewPdfOrders = () => {
    history.push({
      pathname: "/Orders",
      state: {
        pdfURL: "http://localhost:8100/assets/pdf/sample.pdf",
        btnName: "Close",
      },
    });
  };

  const ViewPdfPrescriptions = (date: any) => {
    var pdfEndPoint = PreparePfUrl(date);
    if (Browser) {
      Browser.open({ url: pdfEndPoint });
    } else {
      history.push({
        pathname: "/PdfViewerParent",
        state: {
          pdfURL: pdfEndPoint,
          btnName: "Close",
        },
      });
    }
  };

  const PreparePfUrl = (d: string) => {
    console.log(d);
    var pdfEndPoint =
      endpoints.hostName +
      apiPath.PrescriptionApiPath +
      "&token=" +
      localStorage.getItem("token");
    pdfEndPoint = pdfEndPoint.replace("{0}", PATCODE);
    pdfEndPoint = pdfEndPoint.replace("{1}", d);
    return pdfEndPoint;
  };
  const goToUpdatePAge = (appointment: any) => {
    console.log(appointment);
    history.push({
      pathname:
        "/BookAnAppointmentStep2/" +
        appointment!.clinicCode +
        "/" +
        appointment.transNo,
      // state: {
      //   transNo: appointment!.transNo,
      //   date: appointment!.date,
      //   time:appointment!.time,
      // },
    });
  };
  const cancelAppointment = (appointment: any) => {
    let reqData = {
      APPOINTMENTID: appointment.transNo,
      APPSTATUS: "2",
    };
    let headers = getAuthHeader();

    axios
      .post<any>(
        endpoints.hostName + apiPath.updateAppointmentApiPath,
        reqData,
        { headers }
      ) //"http://34.131.82.122:3001/alemadi/mobile/patient/appointment/patcode/270198?completed=true")
      .then(
        (response) => {
          console.log(response.data);
          presentToastWithOptions(
            "Appointment cancelled successfully",
            "success"
          );

          GetDoctorList();
        },
        (error) => {
          console.error("[your doctor page]: Cancel Appointment Error");
          presentToastWithOptions("something went wrong.", "error");
          handleAuthError(error, history);
        }
      );
  };
  const onClose = () => {
    setShowModal(false);
  };

  return (
    <IonPage>
      <Header title={strings.yourDoctorsPage} defaultRoute={"/Main"} />
      <IonContent>
        {details.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.lastVist} class="cardStyle">
            <IonCardContent class="cardContentStyle">
              <IonGrid>
                <IonRow class="firstRow">
                  <IonCol size="2">
                    <IonImg src={endpoints.hostName + item.iconURL} />
                  </IonCol>
                  <IonCol size="6" class="textCol">
                    <IonText class="text1" className="fontHeading">
                      <b>{item.name} </b>
                    </IonText>
                    <IonText class="text2">{item.department}</IonText>
                  </IonCol>
                  <IonCol size="4" className="status-badge-col">
                    <IonText>
                      <IonBadge
                        color="primary"
                        className={
                          item.status == "Cancelled"
                            ? "red-badge"
                            : "green-badge"
                        }
                      >
                        {strings.status![parseInt(item?.appStatus)]}
                      </IonBadge>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="secondRow">
                  <IonCol>
                    <IonText>
                      <b>{strings.lastVisite}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="thirdRow">
                  <IonCol>
                    <IonText>
                      <b>{item.lastVist}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  {item.status != "Cancelled" ? (
                    item.currentStatus != "0" ? (
                      <IonCol>
                        <IonButton size="small" onClick={ViewPdfDiagnosis}>
                          {strings.diagnosis}
                        </IonButton>
                        &nbsp;&nbsp;&nbsp;
                        <IonButton size="small" onClick={ViewPdfOrders}>
                          {strings.orders}
                        </IonButton>
                        &nbsp;&nbsp;&nbsp;
                        <IonButton
                          size="small"
                          onClick={() => ViewPdfPrescriptions(item.date)}
                        >
                          {strings.prescription}
                        </IonButton>
                      </IonCol>
                    ) : item.currentStatus == "0" ? (
                      <IonCol>
                        <IonButton
                          size="small"
                          onClick={() => {
                            goToUpdatePAge(item);
                          }}
                        >
                          {strings.reschedule}
                        </IonButton>
                        &nbsp;&nbsp;&nbsp;
                        <IonButton
                          size="small"
                          className="redBtn"
                          onClick={() =>
                            present({
                              cssClass: "my-css",
                              header: "Cancel Appointment",
                              message:
                                "Are you sure you want to cancel your appointment?",
                              buttons: [
                                "No",
                                {
                                  text: "Yes",
                                  handler: (_d) => {
                                    cancelAppointment(item);
                                  },
                                },
                              ],
                              onDidDismiss: (e) => console.log("did dismiss"),
                            })
                          }
                        >
                          {strings.cancel}
                        </IonButton>
                        &nbsp;&nbsp;&nbsp;
                      </IonCol>
                    ) : null
                  ) : null}
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
        <IonModal isOpen={showModal} cssClass="diagnosis-modal">
          <IonHeader translucent className="header">
            <IonToolbar className="header">
              <IonTitle className="white-color">Diagnosis </IonTitle>
              <IonButtons slot="end">
                <IonButton onClick={onClose}>Close</IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent fullscreen={false}>
            <IonGrid class="modal-card">
              <IonRow>
                <IonCol class="GridCol">
                  {diagnosisDetails.map((item, i) => (
                    <IonCard className="fontColor cardStyle" key={i}>
                      <IonCardContent style={{ color: "#91918c" }} color="dark">
                        <IonRow>
                          <IonCol size="12" className="font">
                            <b className="fontHeading">ICDCODE: </b>
                            {item.ICDCODE}
                          </IonCol>
                        </IonRow>
                        <IonRow>
                          <IonCol size="12" className="font">
                            <b className="fontHeading">ICDDESC:</b>{" "}
                            {item.ICDDESC}
                          </IonCol>
                        </IonRow>
                      </IonCardContent>
                    </IonCard>
                  ))}
                  <IonLoading
                    isOpen={showLoadingAnimation}
                    message={"Loading..."}
                  />
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonContent>

          {/* <IonButton onClick={() => setShowModal(false)}>Close</IonButton> */}
        </IonModal>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default YourDoctors;

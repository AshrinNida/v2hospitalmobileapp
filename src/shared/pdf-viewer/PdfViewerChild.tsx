import React, { useState } from "react";
import { Document, Page } from "react-pdf/dist/esm/entry.webpack";
import "./PdfViewerChild.css";
import { saveAs } from "file-saver";
import { IonFab, IonFabButton, IonIcon } from "@ionic/react";
import { download, logoTwitter } from "ionicons/icons";
interface PdfProps {
  URL: any;
}

const PdfViewerChild: React.FC<PdfProps> = ({ URL }) => {
  const [numPages, setNumPages] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }: { numPages: any }) {
    console.log("PdfViewerChild setting page number:", numPages);

    setNumPages(numPages);
    setPageNumber(1);
  }

  function changePage(offset: any) {
    setPageNumber((prevPageNumber) => prevPageNumber + offset);
  }

  function previousPage() {
    changePage(-1);
  }

  function nextPage() {
    changePage(1);
  }

  const DownloadButton = () => {
    saveAs(URL, "Alemadi.pdf");
  };
  return (
    <div>
      <div className="pdf_toolbar">
        <div className="navigation_button_container">

          <div
            onClick={previousPage}
            className={
              "material-icons navigation_icon" +
              (pageNumber <= 1 ? " disabled" : "")
            }
          >
            arrow_back
          </div>
          <div>
            {pageNumber || (numPages ? 1 : "--")} of {numPages || "--"}
          </div>
          <div
            onClick={nextPage}
            className={
              "material-icons navigation_icon" +
              (pageNumber >= numPages ? " disabled" : "")
            }
          >
            arrow_forward
          </div>
        </div>
      </div>

      <IonFab slot="fixed" vertical="bottom" horizontal="end">
        <IonFabButton className="download_button" onClick={DownloadButton}>
          <IonIcon icon={download} />
        </IonFabButton>
      </IonFab>

      <Document file={URL} onLoadSuccess={onDocumentLoadSuccess}>
        {/* <Page scale={0.5} pageNumber={pageNumber} /> */}
        <Page  width={300}  pageNumber={pageNumber} />
      </Document>
    </div>
  );
};

export default PdfViewerChild;

import {
    IonButton,
    IonContent,
    IonPage,
} from "@ionic/react";
import React, {  } from "react";
import "./PdfViewerParent.css"

import PdfViewerChild from "./PdfViewerChild";
import { useHistory } from "react-router";

const PdfViewContainer: React.FC = () => {
    let history: any = useHistory();
    let data:any = history.location.state

 
    return (
        <IonPage>
            <IonContent>

                    <div style={{ background: 'white' }}>
                        <PdfViewerChild URL={data.pdfURL}/>
                    </div>
                    <div className="button_container" >
                    <IonButton class="pdfViewer" color="success" onClick={history.goBack}>{data.btnName}</IonButton>
                    </div>
                
            </IonContent>
        </IonPage>


    )
}

export default PdfViewContainer;



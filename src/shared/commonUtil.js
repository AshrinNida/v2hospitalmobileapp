export let getAuthHeader = function () {

  	let authToken = localStorage.getItem('token');
    let headers = { token: `${''}` };
    if(authToken && authToken.length > 0) {
        headers = { token: `${authToken}` };
    }
  	return headers;
  }


export let handleAuthError = function (error,history) {

  	if(error?.response?.status === 401) {
  		history?.push("/login");
  	}
  }



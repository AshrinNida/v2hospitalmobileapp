import Pharmacy from "../pages/Pharmacy"

export const endpoints={
    hostName:'http://localhost:3001',
}
export const testData={
    patCode:'270198',
}

export const apiPath={
    loginApiPath:'/alemadi/mobile/patient/otp/send',
    sendOtpApiPath:'/alemadi/mobile/patient/otp/send',
    verifyOtpApiPath:'/alemadi/mobile/patient/otp/verify',
    validateQIdApiPath:'/alemadi/mobile/patient/verify/qid/',
    registerUserApiPath:'/alemadi/mobile/patient/register',
    getPatientReportApuPath:'/alemadi/mobile/patient/reports/lab/patcode/',
    DoctorsListApiPath:'/alemadi/mobile/doctor/list',
    DoctorAvailabilityDetailsApiPath:'/alemadi/mobile/doctor/timeslots?clinicCode=',
    bookAppointmentApiPath:'/alemadi/mobile/patient/appointment/book',
    PrescriptionApiPath:'/alemadi/mobile/patient/prescription/pdf?patcode={0}&date={1}',
    CompletedAppointmentApiPath:'/alemadi/mobile/patient/appointment/patcode/270198',
    insuranceApiPath: '/alemadi/mobile/patient/insurance/patcode/',
    AppointmentApiPath:'/alemadi/mobile/patient/appointment/patcode/',
    ReportPdfApiPath:'/alemadi/mobile/patient/report/lab/pdf?patcode={0}&date={1}',
    doctorDetailsApiPath:'/alemadi/mobile/doctor/clinicCode/',
    updateAppointmentApiPath:'/alemadi/mobile/patient/appointment/update',
    insuranceListApiPath:'/alemadi/mobile/insurance/names/list',
    ordersApiPath:'/alemadi/mobile/patient/orders/patcode/',
    diagnosisApiPath:'/alemadi/mobile/patient/diagnosis/patcode/',
    diagnosisByAppIdApiPath:'/alemadi/mobile/patient/diagnosis/appid/12345',
    AppointmentByIdApiPath:'/alemadi/mobile/patient/appointment/id/',
    apiPathBarcode: '/alemadi/mobile/patient/charts/barcode/qid/',
    TocApiPath:'/docs/toc.txt',
    PharmacyApiPath: '/alemadi/mobile/pharmacy/search?itemcode=',
}

export const tokenKey={
    AUTH_TOKEN_KEY:'token'
}
  export async function presentToastWithOptions(msg,type) {
   
    let isToastOn=document.getElementsByTagName('ion-toast').length
    console.log('onDidDismiss resolved with role top', document.getElementsByTagName('ion-toast').length);

    if(isToastOn && isToastOn > 0){
      console.log('onDidDismiss resolved with role top', document.getElementsByTagName('ion-toast'));
      return;
    }
   
    const toast = document.createElement('ion-toast');
    // toast.header = 'Toast header';
    toast.color = type=='error'?'danger':'success';
    toast.message = msg;
    toast.position = 'bottom';
    toast.duration=2000;
    

    toast.buttons = [
       {
        text: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ];
  
    document.body.appendChild(toast);
    await toast.present();
    // console.log('onDidDismiss resolved with role', document.getElementsByTagName('ion-toast'));

    const { role } = await toast.onDidDismiss();
  }
 
export  async function toastPromises(msg, type){
  return new Promise((resolve, reject) => {
    
    let isToastOn=document.getElementsByTagName('ion-toast').length
    console.log('onDidDismiss resolved with role top', document.getElementsByTagName('ion-toast').length);

    if(isToastOn && isToastOn > 0){
      console.log('onDidDismiss resolved with role top', document.getElementsByTagName('ion-toast'));
      reject();
      return;
    }
   
    const toast = document.createElement('ion-toast');
    // toast.header = 'Toast header';
    toast.color = type=='error'?'danger':'success';
    toast.message = msg;
    toast.position = 'bottom';
    toast.duration=2000;
    
    toast.buttons = [
       {
        text: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ];
  
    document.body.appendChild(toast);
    toast.present();
    // console.log('onDidDismiss resolved with role', document.getElementsByTagName('ion-toast'));

    const { role } =  toast.onDidDismiss();
    resolve()

 });
}